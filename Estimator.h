#ifndef _ESTIMATOR_HEADER_
#define _ESTIMATOR_HEADER_

#include <cmath>
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <typeinfo>

#include "Particle.h"
#include "Material.h"
#include "Utility.h"
#ifdef _MPI
  #include "mpi.h"
#endif

class Cell;

class Estimator {
  private:
    
  protected:
    unsigned long long nhist;
    unsigned long long ngen;
    std::string estimator_name;
  public:
    Estimator( std::string label ) : estimator_name(label) {};
    ~Estimator() {};

    virtual std::string name() final { return estimator_name; };
    
    virtual void score( Particle* )      = 0;
    virtual void endHistory()            = 0;
    virtual void report( int, int, int ) = 0;
    void   setNHistories( unsigned long long i ) { nhist = i; };
    std::string getName() { return estimator_name; };
	virtual void add_entrant_energy(Particle* p) = 0;
	virtual void add_exit_energy(Particle* p) = 0; 
	virtual void tallyflux( Particle*, double ) = 0;
};

class single_valued_estimator : public Estimator {
  private:

  protected:
    double tally_hist, tally_sum, tally_squared, tally_gen_sum, tally_gen_squared;
  public:
     using Estimator::score;

    single_valued_estimator(std::string label ) : Estimator(label) { 
      nhist         = 0;
      ngen          = 0;
      tally_hist    = 0.0;   
      tally_sum     = 0.0; 
      tally_squared = 0.0;
      tally_gen_sum = 0.0;
      tally_gen_squared = 0.0;
    };
    ~single_valued_estimator() {};

    void endHistory() { 
      nhist++;
      tally_sum     += tally_hist;
      tally_squared += tally_hist * tally_hist;
      tally_hist = 0.0;
    }

    virtual void score( Particle* ) = 0;

    void report( int n_tracks, int rank, int world_size ) {
      double mean = tally_sum / nhist;
      double var  = ( tally_squared / nhist - mean*mean ) / nhist;
      #ifdef _MPI
        double global_var;
        MPI_Reduce(&var,&global_var,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        var /= world_size;
      #endif
      double sdev = std::sqrt( var );
      double sdom = sdev / mean;
      double fom  = 1.0 / ( n_tracks * sdom * sdom );
      #ifdef _MPI
        double global_mean;
        MPI_Reduce(&mean,&global_mean,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        global_mean /= world_size;
        mean = global_mean;
      #endif
      if (rank == 0) { std::cout << name() << "   " << std::scientific << mean << " +- " << sdev << " (" << sdom << ") FOM: " << fom << std::endl; };
    };
	
	virtual void add_entrant_energy(Particle* p) final {};
	virtual void add_exit_energy(Particle* p) final {};
	virtual void tallyflux( Particle*, double ) = 0;

};

class surface_current_estimator : public single_valued_estimator {
  private:

  public:
    surface_current_estimator( std::string label ) : single_valued_estimator(label) {};
    ~surface_current_estimator() {};

    void score( Particle* );
	void tallyflux( Particle*, double ) {};
};

class counting_estimator : public Estimator {
  private:
    
  protected:
    int count_hist;
    std::vector< double > tally;
  public:
    counting_estimator( std::string label ) : Estimator(label) { count_hist = 0; };
    ~counting_estimator() {};

    void score( Particle* );
    void endHistory();
    void report( int, int, int );
	void add_entrant_energy(Particle* p) final {};
	void add_exit_energy(Particle* p) final {};
	void tallyflux( Particle*, double ) final {};
};

class flux_estimator : public single_valued_estimator {
  private:

  protected:
    double cell_volume = 0.0;
    std::string rxn_type = "None";
    double multiplier = 1.0;
    std::vector< std::shared_ptr< Cell > > CellList;
  public:

    flux_estimator( std::string label ) : single_valued_estimator(label) {};
    ~flux_estimator() {};

    virtual void tallyflux( Particle*, double ) = 0;
    void addVolume( double v ) { cell_volume = v; }
    void setReaction( std::string type ) { rxn_type = type; }
    void addCellList( std::vector< std::shared_ptr< Cell > > CL ) { CellList = CL; };
    std::vector< std::shared_ptr< Cell > > getCellList() { return CellList; };
    
    std::pair<double, double> getResults() {
      double mean = tally_sum / nhist;
      double var  = ( tally_squared / nhist - mean*mean ) / nhist;
      #ifdef _MPI
        double global_var;
        MPI_Reduce(&var,&global_var,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        var /= world_size;
        double global_mean;
        MPI_Reduce(&mean,&global_mean,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        global_mean /= world_size;
        mean = global_mean;
      #endif
      return std::make_pair( mean, var );
    }
};

class track_length_estimator : public flux_estimator {
  private:
  public:
    track_length_estimator( std::string label ) : flux_estimator(label) {};
    ~track_length_estimator() {};

    void score( Particle* );
    void tallyflux( Particle*, double );
};

class counting_track_length_estimator : public track_length_estimator {
  private:

  protected:
    std::vector< std::pair< double, double > > data;
    double MinValue;
    double MaxValue;
    double NumberOfIntervals;
    double delta;
    std::string type;
    bool log_flag;
  public:

    counting_track_length_estimator( std::string label, std::string est_type, double min_value, double max_value, double int_size, bool logflag ) :
      track_length_estimator(label), type(est_type), MinValue(min_value), MaxValue(max_value), NumberOfIntervals(int_size), log_flag(logflag) {
      if ( MaxValue - MinValue < 0 ) { std::cout << "Error, max value must be greater than min value for estimator " << estimator_name << "." << std::endl; }
      delta = ( MaxValue - MinValue ) / NumberOfIntervals;
      
      data.push_back( std::make_pair( 0.0, 0.0 ) );
      
      if ( log_flag ) {
        if ( MinValue <= 0.0 ) { std::cout << "Error, min value for estimator " << estimator_name << " cannot be zero (or less)."; throw; }
        std::vector<double> logspace = GenerateLogSpace( MinValue, MaxValue, NumberOfIntervals );
        for ( int i = 0; i <= NumberOfIntervals + 1; i++ ) {
          data.push_back( std::make_pair( logspace[i], 0.0 ) );
        }
      } else {
        for ( double i = MinValue; i <= MaxValue; i += delta ) {
          data.push_back( std::make_pair( i, 0.0 ) );
        }
      }
    };
    ~counting_track_length_estimator() {};

    void tallyflux( Particle*, double );
    void report( int, int, int );
};

class collision_estimator : public flux_estimator {
  private:
  public:
    collision_estimator( std::string label ) : flux_estimator(label) {};
    ~collision_estimator() {};

    void score( Particle* );
    void tallyflux( Particle*, double );
};

class counting_collision_estimator : public collision_estimator {
  private:

  protected:
    std::vector< std::pair< double, double > > data;
    double MinValue;
    double MaxValue;
    double NumberOfIntervals;
    double delta;
    std::string type;
    bool log_flag;
  public:

    counting_collision_estimator( std::string label, std::string est_type, double min_value, double max_value, double int_size, bool logflag ) :
      collision_estimator(label), type(est_type), MinValue(min_value), MaxValue(max_value), NumberOfIntervals(int_size), log_flag(logflag) {
      if ( MaxValue - MinValue < 0 ) { std::cout << "Error, max value must be greater than min value for estimator " << estimator_name << "." << std::endl; }
      delta = ( MaxValue - MinValue ) / NumberOfIntervals;
      
      data.push_back( std::make_pair( 0.0, 0.0 ) );
      
      if ( log_flag ) {
        if ( MinValue <= 0.0 ) { std::cout << "Error, min value for estimator " << estimator_name << " cannot be zero (or less)."; throw; }
        std::vector<double> logspace = GenerateLogSpace( MinValue, MaxValue, NumberOfIntervals );
        for ( int i = 0; i <= NumberOfIntervals + 1; i++ ) {
          data.push_back( std::make_pair( logspace[i], 0.0 ) );
        }
      } else {
        for ( double i = MinValue; i <= MaxValue; i += delta ) {
          data.push_back( std::make_pair( i, 0.0 ) );
        }
      }
    };
    ~counting_collision_estimator() {};

    void tallyflux( Particle*, double );
    void report( int, int, int );
};

class pulse_height_estimator : public Estimator {
private:

protected:
    std::vector< std::pair< double, double > > data;
	std::vector<double> entrant_energy;
	std::vector<double> exit_energy;
  double MinValue;
  double MaxValue;
  double NumberOfIntervals;
  double delta;
  bool log_flag;
	std::vector< std::shared_ptr< Cell > > CellList;
	std::vector<double> logspace;
	std::vector<double> linspace;
public:
	pulse_height_estimator( std::string label, double min_value, double max_value, double int_size, bool logflag ) :
		Estimator(label), MinValue(min_value), MaxValue(max_value), NumberOfIntervals(int_size), log_flag(logflag) {
		
		if ( MaxValue - MinValue < 0 ) { std::cout << "Error, max value must be greater than min value for estimator " << estimator_name << "." << std::endl; }
		delta = ( MaxValue - MinValue ) / NumberOfIntervals;
    
    data.push_back( std::make_pair( 0.0, 0.0 ) );

		if ( log_flag ) {
			if ( MinValue <= 0.0 ) { std::cout << "Error, min value for estimator " << estimator_name << " cannot be zero (or less)."; throw; }
			logspace = GenerateLogSpace( MinValue, MaxValue, NumberOfIntervals );
			for ( int i = 0; i <= NumberOfIntervals + 1; i++ ) {
				data.push_back( std::make_pair( logspace[i], 0.0 ) );
			}
		}else {
			for ( double i = MinValue; i <= MaxValue; i += delta ) {
				data.push_back( std::make_pair( i, 0.0 ) );
				linspace.push_back(i);
			}
		}
		nhist         = 0;
    };
    ~pulse_height_estimator() {};
	
	void addCellList( std::vector< std::shared_ptr< Cell > > CL ) { CellList = CL; };
  
	// we don't want anything to happen in the scoring method
    void score( Particle* )  {};
	
	void add_entrant_energy(Particle* p) { 
		if (p->wgt() != 1.0){
			std::cout << "Error in Pulse Height Estimator: Particle Weight is not 1." << std::endl;
			throw;
		}
		entrant_energy.push_back(p->energy()); 
	};
	
	void add_exit_energy(Particle* p) { 
		if (p->wgt() != 1.0){
			std::cout << "Error in Pulse Height Estimator: Particle Weight is not 1." << std::endl;
			throw;
		}
		exit_energy.push_back(p->energy()); 
	};
	
	// since we are considering the sum of the energy over the entire history and adding
	// that to a bin, the tallying occurs at the end of a history
	// cannot use variance reduction as tallies are only done for weight = 1;
    void endHistory()  {
		// the energy deposited occurs over the entire history
		double energy_deposited = 0.0;
		if (entrant_energy.size() != exit_energy.size()) {
			std::cout << "Error in Pulse Height Estimator: incongruent exit and entrance energies." << std::endl;
			std::cout << "The number of exit and entrant energies must be equivalent in this version." << std::endl;
			throw;
		}
		// add to the energy deposited for the entire history
		double entrant_energy_total = 0.0;
		double exit_energy_total = 0.0;
		for(int i = 0; i < entrant_energy.size(); i++){
			entrant_energy_total += entrant_energy.at(i);
		}
		for(int i = 0; i < exit_energy.size(); i++){
			exit_energy_total += exit_energy.at(i);
		}
		energy_deposited = entrant_energy_total - exit_energy_total;
		// find the index of this deposited energy
		int E_index;
		if(log_flag){
			E_index = bin_search(&logspace, energy_deposited);
		}
		else{
			E_index = bin_search(&linspace, energy_deposited);
		}
		
		// increment the pulse in this bin
		// note that this forces particle weight to 1.0
		// only score if particle actually entered detector
		if(entrant_energy.size() != 0){
			data[E_index].second += 1.0;
		}
		// clear the entrant and exit energies for next history
		entrant_energy.clear();
		exit_energy.clear();
		nhist++;
	};
    void report( int, int, int );
	
	void tallyflux( Particle*, double ) final {};
	
	std::vector< std::pair< double, double > > get_data() {return data;};
};

#endif
