#include <vector>
#include <utility>
#include <memory>
#include <cassert>

#include "Random.h"
#include "Particle.h"
#include "Reaction.h"
#include "Nuclide.h"
#include "Material.h"

// add a Nuclide and atom fraction to the Material
void Material::addNuclide( std::shared_ptr< Nuclide > N, double frac ) {
  N->setMatTempPtr( &Temperature );
  Nuclides.push_back( std::make_pair( N, frac ) ); 
}

// add a Fissionable Nuclide and atom fraction to the Material
void Material::addFissionableNuclide( std::shared_ptr< Nuclide > N, double frac ) {
  N->setMatTempPtr( &Temperature );
  FissionableNuclides.push_back( std::make_pair( N, frac ) ); 
}

// private utility function that returns sum of atomic fraction * microscopic total xs
// multiply this by atomic density to get macroscopic cross section
// (this function is useful to accelerate sampling the Nuclide)
double Material::micro_xs(Particle* p) {
  double xs = 0.0;
  for ( auto n : Nuclides ) { 
    // first is pointer to Nuclide, second is atomic fraction
    xs += n.first->total_xs(p) * n.second;
  }
  return xs;
}

// return the macroscopic cross section
double Material::macro_xs(Particle* p) {
  return atom_density() * micro_xs(p);
}

// return the macroscopic absorption cross section
double Material::macro_abs_xs(Particle* p) {
  double micro_abs_xs = 0.0;
  for ( auto n : Nuclides ) {
    for ( auto r : n.first->getReactions() ) {
      if (r->name() == "Capture") {
        micro_abs_xs+=r->xs(p) * n.second;
      }
    }
  }
  return atom_density() * micro_abs_xs;
}

// return the macroscopic scatter cross section
double Material::macro_scat_xs(Particle* p) {
  double micro_scat_xs = 0.0;
  for ( auto n : Nuclides ) {
    for ( auto r : n.first->getReactions() ) {
      if (r->name() == "Scatter") {
        micro_scat_xs+=r->xs(p) * n.second;
      }
    }
  }
  return atom_density() * micro_scat_xs;
}

// return the macroscopic fission cross section
double Material::macro_fiss_xs(Particle* p) {
  double micro_fiss_xs = 0.0;
  for ( auto n : FissionableNuclides ) {
    for ( auto r : n.first->getReactions() ) {
      if (r->name() == "Fission") {
        micro_fiss_xs+=r->xs(p) * n.second;
      }
    }
  }
  return atom_density() * micro_fiss_xs;
}

double Material::getReactionXS(Particle* p, std::string rxname) {
  double micro_xs = 0.0;
  for ( auto n : Nuclides ) {
    for ( auto r : n.first->getReactions() ) {
      if (r->name() == rxname) {
        micro_xs+=r->xs(p) * n.second;
      }
    }
  }
  return atom_density() * micro_xs;
}

// randomly sample a Nuclide based on total cross sections and atomic fractions
std::shared_ptr< Nuclide > Material::sample_Nuclide(Particle* p) {
  double u = micro_xs(p) * Urand();
  double s = 0.0;
  for ( auto n : Nuclides ) {
    // first is pointer to Nuclide, second is atomic fraction
    s += n.first->total_xs(p) * n.second;
    if ( s > u ) { return n.first; }
  }
  assert( false ); // should never reach here
  return nullptr;
}

// randomly sample a fission energy based on nuclide nu fission macroscopic cross section
double Material::sampleFissionEnergy(Particle* p) {
  double total_nufiss = 0.0;
  for ( auto n : FissionableNuclides ) { total_nufiss += n.first->fiss_xs(p) * n.second * atom_density() * n.first->getNubar(); }
  double xi = Urand();
  double s = 0.0;
  for ( auto n : FissionableNuclides ) {
    // first is pointer to Nuclide, second is atomic fraction
    s += n.first->fiss_xs(p) * n.second * atom_density() * n.first->getNubar() / total_nufiss;
    if ( s > xi ) { return n.first->fiss_energy(); }
  }
  assert( false ); // should never reach here
  return 0;
}

// get material nu * Sigma_f based on fissionable isotopes
double Material::getNuSigf( Particle* p ) {
  double total_nu_sigf = 0.0;
  for ( auto n : FissionableNuclides ) {
    total_nu_sigf += n.first->getNubar() * n.first->fiss_xs( p ) * n.second;
  }
  return total_nu_sigf * atom_density();
}

// function that samples an entire collision: sample Nuclide, then its reaction, 
// and finally process that reaction with input pointers to the working Particle p
// and the Particle bank
void Material::sample_collision( Particle* p, std::stack<Particle>* bank ) {
  // first sample Nuclide
  std::shared_ptr< Nuclide >  N = sample_Nuclide(p);
  
  if (N->getImplicitCapture()) // implicit capture
  {
    double xs_cap  = N->cap_xs(p);
    double xs_scat = N->scat_xs(p);
    p->adjustWeight(1 - xs_cap/(xs_cap + xs_scat));
    
    std::shared_ptr< reaction > R = N->get_scatter_reaction();
    R->sample( p, bank );
  }
  else
  {
    // now get the reaction
    std::shared_ptr< reaction > R = N->sample_reaction(p);
    R->sample( p, bank );
  }
}
