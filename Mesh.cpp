#include "Mesh.h"

bool CartesianMesh::in_mesh( Particle* p ) {
  if ( p->pos().x <= origin_point[0] || p->pos().x >= end_point[0]
    || p->pos().y <= origin_point[1] || p->pos().y >= end_point[1]
    || p->pos().z <= origin_point[2] || p->pos().z >= end_point[2] ) {
    return false;
  } else {
    return true;
  }
}

bool CylindricalMesh::in_mesh( Particle* p ) {
  double corrected_x = p->pos().x - origin_point[0];
  double corrected_y = p->pos().y - origin_point[1];
  double corrected_z = p->pos().z - origin_point[2];
  
  double particle_r     = std::sqrt( corrected_x * corrected_x + corrected_y * corrected_y );
  double particle_z     = corrected_z;
  
  if ( particle_r >= extent[0] || particle_z <= origin_point[2] || particle_z >= extent[1] ) {
    return false;
  } else {
    return true;
  }
}

int CartesianMesh::lookup_bin( Particle* p ) {
  int x_skip = std::floor( ( p->pos().x - origin_point[0] ) / spatial_bin_widths[0] );
  int y_skip = std::floor( ( p->pos().y - origin_point[1] ) / spatial_bin_widths[1] ) * spatial_dimensions[0];
  int z_skip = std::floor( ( p->pos().z - origin_point[2] ) / spatial_bin_widths[2] ) * spatial_dimensions[0] * spatial_dimensions[1];
  
  return x_skip + y_skip + z_skip;
}

int CylindricalMesh::lookup_bin( Particle* p ) {
  double corrected_x = p->pos().x - origin_point[0];
  double corrected_y = p->pos().y - origin_point[1];
  double corrected_z = p->pos().z - origin_point[2];
  
  double particle_r     = std::sqrt( corrected_x * corrected_x + corrected_y * corrected_y );
  double particle_z     = corrected_z;
  double particle_theta = std::atan( std::abs( corrected_y / corrected_x ) );
  if ( corrected_x < 0.0 && corrected_y > 0.0 ) {
    particle_theta = pi - particle_theta;
  } else if ( corrected_x < 0.0 && corrected_y < 0.0 ) {
    particle_theta = pi + particle_theta;
  } else if ( corrected_x > 0.0 && corrected_y < 0.0 ) {
    particle_theta = twopi - particle_theta;
  }
  
  int r_level = std::floor( ( particle_r ) / spatial_bin_widths[0] );
  
  int r_skip     = r_level * spatial_dimensions[2];
  int z_skip     = std::floor( ( particle_z ) / spatial_bin_widths[1] ) * spatial_dimensions[0] * spatial_dimensions[2];
  int theta_skip = std::floor( ( particle_theta ) / spatial_bin_widths[2] );
  
  int bin_num = r_skip + z_skip + theta_skip;
  element_volumes[bin_num] = spatial_bin_widths[1] * pi * ( std::pow( ( r_level + 1 ) * spatial_bin_widths[0], 2 ) - std::pow( r_level * spatial_bin_widths[0], 2 ) ) / spatial_dimensions[2];
  return bin_num;
}

std::pair< std::vector< std::vector< double > >, std::vector<double> > CartesianMesh::get_points( int bin_number ) {
  std::vector< std::vector< double > > bounds;
  
  int z_level = std::floor( bin_number / ( spatial_dimensions[0] * spatial_dimensions[1] ) );
  int y_level = std::floor( ( bin_number - z_level * spatial_dimensions[0] * spatial_dimensions[1] ) / spatial_dimensions[0] );
  int x_level = bin_number - y_level * spatial_dimensions[0] - z_level * spatial_dimensions[0] * spatial_dimensions[1];
  
  bounds.push_back( { origin_point[0] + spatial_bin_widths[0] * x_level, origin_point[0] + spatial_bin_widths[0] * ( x_level + 1 ) } );
  bounds.push_back( { origin_point[1] + spatial_bin_widths[1] * y_level, origin_point[1] + spatial_bin_widths[1] * ( y_level + 1 ) } );
  bounds.push_back( { origin_point[2] + spatial_bin_widths[2] * z_level, origin_point[2] + spatial_bin_widths[2] * ( z_level + 1 ) } );
  
  std::vector<double> bin_point;
  for ( int i = 0 ; i < 3 ; i++ ) { bin_point.push_back( ( bounds[i][0] + bounds[i][1] ) / 2 ); }
  
  return std::make_pair( cartesian_product( bounds ), bin_point );
}

std::pair< std::vector< std::vector< double > >, std::vector<double> > CylindricalMesh::get_points( int bin_number ) {
  std::vector< std::vector< double > > bounds;
  
  int z_level     = std::floor( bin_number / ( spatial_dimensions[0] * spatial_dimensions[2] ) );
  int r_level     = std::floor( ( bin_number - z_level * spatial_dimensions[0] * spatial_dimensions[2] ) / spatial_dimensions[2] );
  int theta_level = bin_number - r_level * spatial_dimensions[2] - z_level * spatial_dimensions[0] * spatial_dimensions[2];
  
  bounds.push_back( { spatial_bin_widths[0] * r_level, spatial_bin_widths[0] * ( r_level + 1 ) } );
  bounds.push_back( { spatial_bin_widths[1] * z_level, spatial_bin_widths[1] * ( z_level + 1 ) } );
  bounds.push_back( { spatial_bin_widths[2] * theta_level, spatial_bin_widths[2] * ( theta_level + 1 ) } );
  
  std::vector<double> bin_point;
  for ( int i = 0 ; i < 3 ; i++ ) { bin_point.push_back( ( bounds[i][0] + bounds[i][1] ) / 2 ); }
  
  std::vector< std::vector< double > > points = cartesian_product( bounds );
  bin_point = convertPtToCartesian( bin_point );
  for ( int i ; i < points.size() ; i++ ) { points[i] = convertPtToCartesian( points[i] ); }
  return std::make_pair( points, bin_point );
}

void Mesh::writeMesh( std::ofstream* file ) {
  *file << "      <Points>\n";
  *file << "        <DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n";
  for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
    std::pair< std::vector< std::vector< double > >, std::vector<double> > points = get_points( i );
    for ( unsigned long long j = 0 ; j < 8 ; j++ ) {
      *file << "          ";
      for ( unsigned long long k = 0 ; k < 3 ; k++ ) {
        *file << points.first[j][k] << " ";
      }
      if ( i < number_of_elements ) { *file << "\n"; };
    }
  }
  *file << "        </DataArray>\n";
  *file << "      </Points>\n";
}

void Mesh::writeConnectivity( std::ofstream* file ) {
  unsigned long long c = 0;
  *file << "        <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n";
  for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
    *file << "          ";
    for ( unsigned long long j = 0 ; j < 8 ; j++ ) {
      *file << c;
      c++;
      if ( j < 7 ) {
        *file << " ";
      } else {
        *file << "\n";
      }
    }
  }
  *file << "        </DataArray>\n";
}

void Mesh::writeOffsets( std::ofstream* file ) {
  unsigned long long c = 8;
  *file << "        <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n";
  for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
    *file << "          " << c << "\n";
    c += 8;
  }
  *file << "        </DataArray>\n";
}

void Mesh::writeTypes( std::ofstream* file ) {
  *file << "        <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n";
  for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
    *file << "          11\n";
  }
  *file << "        </DataArray>\n";
}

void Mesh::writeTallies( std::ofstream* file, int rank, int world_size ) {
  for ( auto t : TallyList ) {
    *file << "        <DataArray type=\"Float32\" Name=\"" + t-> getTallyName() + "\" format=\"ascii\">\n";
    for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
      double mean = t->tally_sum[i] / nhist;
      #ifdef _MPI
        double global_mean;
        MPI_Reduce(&mean,&global_mean,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        global_mean /= world_size;
        mean = global_mean;
      #endif
      *file << "          " << mean << "\n";
    }
    *file << "        </DataArray>\n";
  }
}

void Mesh::writeErrors( std::ofstream* file, int rank, int world_size ) {
  for ( auto t : TallyList ) {
    if ( rank == 0 ) { *file << "        <DataArray type=\"Float32\" Name=\"StdDev_" + t->getTallyName() + "\" format=\"ascii\">\n"; }
    for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
      double mean = t->tally_sum[i] / nhist;
      double var = ( t->tally_squared[i] / nhist - mean*mean ) / nhist;
      #ifdef _MPI
        double global_var;
        MPI_Reduce(&var,&global_var,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        var /= world_size;
      #endif
      double sdev = std::sqrt( var );
      double sdom = sdev / mean;
      // double fom  = 1.0 / ( n_tracks * sdom * sdom );
      if ( rank == 0 ) { *file << "          " << sdom << "\n"; }
    }
    if ( rank == 0 ) { *file << "        </DataArray>\n"; }
  }
}

void Mesh::closeFile( std::ofstream* file ) {
  *file << "    </Piece>\n";
  *file << "  </UnstructuredGrid>\n";
  *file << "</VTKFile>";
}

void Mesh::writeVTUFile( int n_tracks, int rank, int world_size ) {
  if ( rank == 0 ) { std::cout << "Writing mesh data to " << file_name + ".vtu" << "..." << std::endl; }
  std::ofstream output_file;
  if ( rank == 0 ) { output_file.open( file_name + ".vtu" ); }
  
  if ( rank == 0 ) {
    output_file << "<?xml version=\"1.0\"?>\n";
    output_file << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
    output_file << "  <UnstructuredGrid>\n";
    output_file << "    <Piece NumberOfPoints=\"" << 8*number_of_elements << "\" NumberOfCells=\"" << number_of_elements << "\">\n";
    writeMesh( &output_file );
    output_file << "      <Cells>\n";
    writeConnectivity( &output_file );
    writeOffsets( &output_file );
    writeTypes( &output_file );
    output_file << "      </Cells>\n";
    output_file << "      <CellData Scalars=\"Result\">\n";
  }
  writeTallies( &output_file, rank, world_size );
  writeErrors( &output_file, rank, world_size );
  if ( rank == 0 ) {
    output_file << "      </CellData>\n";
    closeFile( &output_file );
    output_file.close();
  }
}

void CartesianMesh::writeRawData( std::ofstream* file, int rank, int world_size ) {
  if ( rank == 0 ) {
    *file << std::setw(13) << "x " << std::setw(13) << "y " << std::setw(13) << "z ";
    for ( auto t : TallyList ) { *file << std::setw(13) << t->getTallyName() + " " << std::setw(13) << "StdDev "; };
  }
  if ( rank == 0 ) { *file << "\n"; }
  for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
    std::pair< std::vector< std::vector< double > >, std::vector<double> > points = get_points( i );
    for ( int j = 0 ; j < 3 ; j++ ) { if ( rank == 0 ) { *file << std::scientific << points.second[j] << " "; }; }
    for ( auto t : TallyList ) {
      double mean = t->tally_sum[i] / nhist;
      #ifdef _MPI
        double global_mean;
        MPI_Reduce(&mean,&global_mean,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        global_mean /= world_size;
        mean = global_mean;
      #endif
      double var = ( t->tally_squared[i] / nhist - mean*mean ) / nhist;
      #ifdef _MPI
        double global_var;
        MPI_Reduce(&var,&global_var,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        var /= world_size;
      #endif
      double sdev = std::sqrt( var );
      double sdom = sdev / mean;
      if ( rank == 0 ) { *file << std::scientific << mean << " " << sdom << " "; }
    }
    if ( rank == 0 ) { *file << "\n"; }
  }
}

void CylindricalMesh::writeRawData( std::ofstream* file, int rank, int world_size ) {
  if ( rank == 0 ) {
    *file << std::setw(13) << "r " << std::setw(13) << "z " << std::setw(13) << "t ";
    for ( auto t : TallyList ) { *file << std::setw(13) << t->getTallyName() + " " << std::setw(13) << "StdDev "; };
  }
  if ( rank == 0 ) { *file << "\n"; }
  for ( unsigned long long i = 0 ; i < number_of_elements ; i++ ) {
    std::pair< std::vector< std::vector< double > >, std::vector<double> > points = get_points( i );
    std::vector<double> cyl_point = convertPtToCylindrical( points.second );
    for ( int j = 0 ; j < 3 ; j++ ) { if ( rank == 0 ) { *file << std::scientific << cyl_point[j] << " "; }; }
    for ( auto t : TallyList ) {
      double mean = t->tally_sum[i] / nhist;
      #ifdef _MPI
        double global_mean;
        MPI_Reduce(&mean,&global_mean,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        global_mean /= world_size;
        mean = global_mean;
      #endif
      double var = ( t->tally_squared[i] / nhist - mean*mean ) / nhist;
      #ifdef _MPI
        double global_var;
        MPI_Reduce(&var,&global_var,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
        var /= world_size;
      #endif
      double sdev = std::sqrt( var );
      double sdom = sdev / mean;
      if ( rank == 0 ) { *file << std::scientific << mean << " " << sdom << " "; }
    }
    if ( rank == 0 ) { *file << "\n"; }
  }
}

void Mesh::writeRawFile( int n_tracks, int rank, int world_size ) {
  // if ( rank == 0 ) { std::cout << "Writing mesh data to " << file_name + ".out" << "..." << std::endl; }
  std::ofstream output_file;
  if ( rank == 0 ) { output_file.open( file_name + ".out" ); }
  writeRawData( &output_file, rank, world_size );
  if ( rank == 0 ) { output_file.close(); }
}

void Mesh::report( int n_tracks, int rank, int world_size ) {
  if ( output_type == "vtu" ) {
    writeVTUFile( n_tracks, rank, world_size );
  } else if ( output_type == "raw" ) {
    writeRawFile( n_tracks, rank, world_size );
  }
}