#ifndef _PARTICLE_HEADER_
#define _PARTICLE_HEADER_

#include <memory>
#include <cmath>

#include "Point.h"

class Cell;

class Particle {
  private:
    point p_pos, p_dir;
    double k = 8.6173303e-11; // Boltzmann constant in MeV/K
    double pi = std::acos(-1.0); // Pi constant
    double p_wgt;
    double p_energy; // Particle energy in MeV
    double p_speed;
    double p_time;
    double p_mass = 1.674929e-27; // Neutron mass (kg)
    double p_amu = 1.0086654;
    double conv_const = 1.60218e-27; // [ kg*(cm/ns)^2 ] / [ MeV ]
    bool   exist;
    std::shared_ptr< Cell > p_cell;
  public:
     Particle( point p, point d, double en, double t );
    ~Particle() {};

    point      pos() { return p_pos; };    // return Particle position
    point      dir() { return p_dir; };    // return Particle direction 
    double     wgt() { return p_wgt; };   // return Particle weight
    double  energy() { return p_energy; };   // return Particle energy
    double   speed() { return p_speed; };
    double getTime() { return p_time; };   // return Particle time (ns)
    bool     alive() { return exist; };   // return Particle state flag
    void setEnergy( double E ) { p_energy = E; };

    ray getRay() { return ray( p_pos, p_dir ); }
    double time_traveled( double d );
    double dist_traveled( double t );

    std::shared_ptr< Cell > cellPtr() { return p_cell; }

    void move( double s );
    void scatter( double mu0 );
    void free_gas_scatter( double mu0, double* A, double* T );
    void kill();
    void setDirection( point p );
    void adjustWeight( double f );
    void recordCell( std::shared_ptr< Cell > cel );
	void set_energy(double e_in) {p_energy = e_in;};
};

#endif
