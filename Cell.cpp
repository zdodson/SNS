#include <iostream>
#include <vector>
#include <utility>
#include <memory>
#include <cassert>
#include <cmath>
#include <limits>

#include "Cell.h"
#include "Particle.h"

// take in a pair of Surface pointer and integer describing sense (must not be zero!)
// and append to vector of Surfaces
void Cell::addSurface( std::shared_ptr< Surface > S, int sense ) {

  assert( sense != 0 );
  int sgn = std::copysign( 1, sense );

  Surfaces.push_back( std::make_pair( S, sgn ) );
}

// test if point p inside the current Cell
bool Cell::testPoint( point p ) {

  // loop over Surfaces in Cell, if not on correct side return false
  // if on correct side of all Surfaces, Particle is in the Cell and return true
  for ( auto s : Surfaces ) {
    // first = Surface pointer, second = +/- 1 indicating sense
    if ( s.first->eval( p ) * s.second < 0 ) { return false; }  
  }
  return true;
}

// find first intersecting Surface of ray r and distance to intersection
std::pair< std::shared_ptr< Surface >, double > Cell::SurfaceIntersect( ray r ) {
  double dist = std::numeric_limits<double>::max();
  std::shared_ptr< Surface > S = nullptr;
  for ( auto s : Surfaces ) {
    // first is a Surface pointer; distance is always positive or huge if invalid
    double d = s.first->distance( r );
    if ( d < dist ) {
      // current Surface intersection is closer
      dist = d;
      S    = s.first;
    }
  }
  return std::make_pair( S, dist );
}

// move particle and tally all track length estimators
void Cell::moveParticle( Particle* p, double s, bool active_flag ) {
  if ( active_flag ) { for ( auto e : Cell_Estimators ) { e->tallyflux( p, s ); } }
  p->move( s );
}

// Sample collision in cell material. Bank is passed in for fission interactions.
void Cell::sampleCollision( Particle* p, std::stack<Particle>* bank, double timelimit, bool active_flag , double s ) {

  if ( p->cellPtr()->getExponentialTransform() ) {
      double alpha   = p->cellPtr()->getTransformParameter();
      double totalXS = p->cellPtr()->macro_xs( p );
      double mu      = std::atan( p->dir().y / p->dir().x );
      double wc      = std::exp( -alpha * totalXS * mu * s );
      p->adjustWeight( wc );
  } // end exponential transfrom loop

  if ( active_flag ) {
    for ( auto e : Cell_Estimators ) { e->score( p ); };
  }
  Cell_Material->sample_collision( p, bank );
  if ( p->getTime() > timelimit ) {
    p->kill();
  }
  if(!p->alive()){
	for(auto e : p->cellPtr()->getEstimators()){
	  e->add_exit_energy(p);
	}
  }
}

