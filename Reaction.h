#ifndef _REACTION_HEADER_
#define _REACTION_HEADER_

#include <vector>
#include <iostream>
#include <string>
#include <memory>
#include <stack>
#include <utility>
#include <fstream>

#include "Particle.h"
#include "Distribution.h"
#include "Random.h"
#include "Utility.h"
#include "OTF_data.h"

class Nuclide;

class FBotfdata;

class reaction {
  private:
    double const_xs;
    double energy_dependent_xs;
    std::string cross_section_filename;
    bool is_tabular_data;
    
    std::vector<double> energy_table;
    std::vector<double> xs_table;
    
    int otf_ix;
    bool otf_disabled;
  protected:
    std::string rxn_name;
    double* MatTempPtr = nullptr;
    std::shared_ptr<FBotfdata> otf_data_ptr = nullptr;
    
  public:
     reaction( double a, double b, std::string xs_filename, int ind, std::shared_ptr<FBotfdata> FB, bool disab );
    ~reaction() {};

    virtual std::string name() final { return rxn_name; };
    virtual void setMatTempPtr( double * T ) { MatTempPtr = T; };
    virtual double xs(Particle* p) final 
    {
		if ( ( otf_disabled == false) && (p->energy() < otf_data_ptr->get_emax() ) && (p->energy() > otf_data_ptr->get_emin() ) && (*MatTempPtr < otf_data_ptr->get_tmax() ) && (*MatTempPtr > otf_data_ptr->get_tmin() ) )
		  { //USE OTF ROUTINES
		   double otftemp = *MatTempPtr;
//			std::cout << otf_data_ptr->calc_xs(1,0.00000662,1000) << std::endl;
//			std::cout << otf_data_ptr->calc_xs(2,0.00000662,1000) << std::endl;
//			std::cout << otf_data_ptr->calc_xs(101,0.00000662,1000) << std::endl;

		   return otf_data_ptr->calc_xs(otf_ix, p->energy(), otftemp);
		  }
                else {
		// check flag if the cross section should be generated from data
		if (is_tabular_data) {
			int Eindex = bin_search( &energy_table, p->energy() );
			// check if energy exceeds either boundary
			if ((Eindex == 0) || (Eindex == energy_table.size() - 1)){
				return xs_table.at(Eindex);
			}
			// within the energy table
			// perform a linear interpolation
			else
			{
				double frac = (xs_table.at(Eindex) - xs_table.at(Eindex-1))/(energy_table.at(Eindex) - energy_table.at(Eindex-1)) * (p->energy() - energy_table.at(Eindex-1)) + xs_table.at(Eindex-1);
				return frac;
			}
		}
		// cross section generated with analytical expression
		else { 
			return const_xs + energy_dependent_xs / std::sqrt(p->energy()); 
			}
		}
    };

    virtual void sample( Particle* p, std::stack< Particle >* bank ) = 0;
};

class capture_reaction : public reaction {
  private:
 
  public:
     capture_reaction( double p1, double p2, std::string xsfile, int indc, std::shared_ptr<FBotfdata> FB, bool disabc ) : reaction(p1,p2,xsfile,indc,FB,disabc) { rxn_name = "Capture"; };
    ~capture_reaction() {};

    void sample( Particle* p, std::stack< Particle >* bank );
};

class scatter_reaction : public reaction {
  private:
    
  protected:
    std::shared_ptr< Distribution<double> > scatter_dist;
  public:
     scatter_reaction( double a, double b, std::string xs_filename, std::shared_ptr< Distribution<double> > D, int inds, std::shared_ptr<FBotfdata> FB, bool disabs ) :
       reaction(a,b,xs_filename,inds,FB,disabs), scatter_dist(D) { rxn_name = "Scatter"; };
    ~scatter_reaction() {};

    void sample( Particle* p, std::stack< Particle >* bank );
};

class free_gas_scatter_reaction : public scatter_reaction {
  private:
    double* A;
  public:
     free_gas_scatter_reaction( double a, double b, std::string xs_filename, std::shared_ptr< Distribution<double> > D, double* AN, int indfgs, std::shared_ptr<FBotfdata> FB, bool disabfgs ) :
       scatter_reaction(a,b,xs_filename,D,indfgs,FB,disabfgs), A(AN) { rxn_name = "Scatter"; };
    ~free_gas_scatter_reaction() {};

    void sample( Particle* p, std::stack< Particle >* bank );
};

class fission_reaction : public reaction {
  private:
    std::shared_ptr< Distribution<int> >   multiplicity_dist; 
    std::shared_ptr< Distribution<point> > isotropic;
    std::shared_ptr< Distribution<double> > chi_dist;
    std::string problem_type;
    double* keffPtr;
  public:
     fission_reaction( double a, double b, std::string xs_filename, std::shared_ptr< Distribution<int> > D, int indf, std::shared_ptr<FBotfdata> FB, bool disabf) :
       reaction(a,b,xs_filename,indf,FB,disabf), multiplicity_dist(D) { 
         rxn_name = "Fission";
         isotropic = std::make_shared< isotropicDirection_Distribution > ( "isotropic" ); 
       };
    ~fission_reaction() {};

    void sample( Particle* p, std::stack< Particle >* bank );
    void setChiDistribution( std::shared_ptr< Distribution<double> > dist ) { chi_dist = dist; };
    double sampleFissionE() {
      if ( chi_dist ) {
        return chi_dist->sample();
      } else {
        return 2.0;
      }
    }
    void setProblemType( std::string type ) { problem_type = type; };
    double getnubar() { return multiplicity_dist->get_Nubar(); };
};

#endif
