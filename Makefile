exec    = MCrun
cc      = g++
opt     = -O3
cflags  = -std=c++11 $(opt)
main    = Main.cpp
testdir = Testing
objects = $(patsubst %.cpp,%.o,$(filter-out $(main), $(wildcard *.cpp)))

ifeq ($(cc),mpic++)
	cflags  += -D_MPI
endif

.PHONY : all test clean

all :	$(objects) $(exec)

test :
	@ cd $(testdir) && $(MAKE)

%.o : %.cpp
	$(cc) $(cflags) -c $<

$(exec) : $(main)
	@rm -f $(exec)
	$(cc) $(cflags) $(objects) $< -o $@

clean :
	rm -f $(objects) $(exec)
	@ cd $(testdir) && $(MAKE) clean
