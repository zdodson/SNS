#ifndef _CELL_HEADER_
#define _CELL_HEADER_

#include <string>
#include <vector>
#include <utility>
#include <memory>

#include "Point.h"
#include "Surface.h"
#include "Material.h"
#include "Estimator.h"
#include "Cell.h"
#include "Particle.h"

class Cell {
  private:
    std::string Cell_name;

    std::vector< std::pair< std::shared_ptr< Surface >, int > > Surfaces;
    std::vector< std::shared_ptr< Cell > > NeighborList;
    std::shared_ptr< Material > Cell_Material;
    std::shared_ptr<exponential_Distribution> Exp;  // Exponential dist.
    std::vector< std::shared_ptr< Estimator > > Cell_Estimators;

    double importance;
    double volume;
    
    bool Forced_Collision;
    bool Weight_Window;
    double upperbound_weight;
	double lowerbound_weight; 

    // Exponential Transform parameters
    bool Exponential_Transform;
    double alpha;
    
  public:
     Cell( std::string label ) : Cell_name(label) { importance = 1.0; Forced_Collision = false; Weight_Window = false;
                                                    Exponential_Transform = false; alpha = 0.0; };
    ~Cell() {};

    std::string name() { return Cell_name; };

    void setMaterial( std::shared_ptr< Material > M ) {
      Cell_Material = M;
      Exp = std::make_shared<exponential_Distribution> ( "Dist_to_Coll" );
    };
    
    std::shared_ptr< Material > getMaterial() { return Cell_Material; }

    void setImportance( double imp ) { importance = imp; };
    double getImportance() { return importance; }

    std::vector< std::shared_ptr< Estimator > > getEstimators() { return Cell_Estimators; };
    std::vector< std::pair< std::shared_ptr< Surface >, int > > getSurfaces() { return Surfaces; };
    std::vector< std::shared_ptr< Cell > > getNeighbors() { return NeighborList; };

    void addSurface( std::shared_ptr< Surface > S, int sense );
    void addNeighbor( std::shared_ptr< Cell > C ) { NeighborList.push_back( C ); };

    void attachEstimator( std::shared_ptr< Estimator > E ) {
      Cell_Estimators.push_back( E );
    };

    double getVolume() { return volume; }

    bool testPoint( point p );
    std::pair< std::shared_ptr< Surface >, double > SurfaceIntersect( ray r );

    double macro_xs(Particle* p) {
      if ( Cell_Material ) { return getMaterial()->macro_xs(p); }
      else { return 0.0; }
    };

    void moveParticle( Particle* p, double s, bool active_flag );
    void sampleCollision( Particle* p, std::stack<Particle>* bank, double timelimit, bool active_flag , double s );
    
    double sampleDistColl(Particle* p) {
      double lambda = Cell_Material->macro_xs(p);
      if ( getExponentialTransform() ) {
        double mu = std::atan( p->dir().y / p->dir().x );
        lambda *= ( 1 - alpha * mu );
      }
      Exp->set_lambda( lambda );
      return Exp->sample();
    };

    void setVolume( double vol ) { volume = vol; };
    
    void setForcedCollision(bool inForcedCollision)  { Forced_Collision = inForcedCollision;};
    bool getForcedCollision() {return Forced_Collision;};
    
     void setWieghtWindow(bool HeavyWieght)  { Weight_Window = HeavyWieght;};
    bool getWeightWindow() {return Weight_Window ;};
	
	void setUpperbound_weight(double Heavy_Weigh_in) {upperbound_weight = Heavy_Weigh_in; };
	void setLowerbound_weight(double Light_Weigh_in) {lowerbound_weight = Light_Weigh_in; };
	
	double getUpperbound_weight(){return upperbound_weight;};
	double getLowerbound_weight(){return lowerbound_weight;};

    void setExponentialTransform( bool ExpTran )  { Exponential_Transform = ExpTran; };
    bool getExponentialTransform()  { return Exponential_Transform; };
    void setTransformParameter( double a )  { alpha = a; };
    double getTransformParameter()  { return alpha; };
};

#endif
