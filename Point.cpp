#include <cmath>

#include "Point.h"

// used to normalize a direction
void point::normalize() {
  double norm = 1.0 / std::sqrt( x*x + y*y + z*z );
  x *= norm; y *= norm; z *= norm;
}

double point::magnitude() {
  return std::sqrt( x * x + y * y + z * z );
}

ray::ray( point p, point d ) : pos(p), dir(d) { dir.normalize(); }
