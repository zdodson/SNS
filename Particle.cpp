#include <cmath>
#include <limits>
#include <iostream>

#include "Particle.h"
#include "Random.h"

// constructor for a new Particle
Particle::Particle( point p, point d, double en, double t ) : p_pos(p), p_dir(d), p_energy(en), p_time(t) {
  p_dir.normalize();
  exist = true;
  p_wgt = 1.0;
  p_cell = nullptr;
  p_speed = std::sqrt( 2 * p_energy * conv_const / p_mass );
}

// move the Particle along its current trajectory
void Particle::move( double s ) {
  p_pos.x += s * p_dir.x;
  p_pos.y += s * p_dir.y;
  p_pos.z += s * p_dir.z;
  p_time  += s / std::pow( 2.0 * p_energy * conv_const / p_mass, 1.0 / 2.0 );
}

// scatter Particle given input direction cosine cos_t0 = mu0
void Particle::scatter( double cos_t0 ) {
  // sample a random azimuthal angle uniformly
  double azi     = 2.0 * pi * Urand();
  double cos_azi = std::cos(azi);
  double sin_azi = std::sin(azi);

  // rotate the local Particle coordinate system aligned along the incident direction
  // to the global problem (x,y,z) coordinate system 
  double sin_t  = std::sqrt( 1.0 - p_dir.z  * p_dir.z  );
  double sin_t0 = std::sqrt( 1.0 - cos_t0 * cos_t0 );

  point q;

  if ( sin_t > std::numeric_limits<double>::epsilon() * 1000.0 ) {
    double c = sin_t0 / sin_t;
    q.x = p_dir.x * cos_t0 + ( p_dir.x * p_dir.z * cos_azi - p_dir.y * sin_azi ) * c;
    q.y = p_dir.y * cos_t0 + ( p_dir.y * p_dir.z * cos_azi + p_dir.x * sin_azi ) * c;
    q.z = p_dir.z * cos_t0 - cos_azi * sin_t0 * sin_t;
  }
  else {
    // if incident direction along z, reorient axes to avoid division by zero
    sin_t  = std::sqrt( 1.0 -  p_dir.y * p_dir.y );
    double c = sin_t0 / sin_t;
    q.x = p_dir.x * cos_t0 + ( p_dir.x * p_dir.y * cos_azi + p_dir.z * sin_azi ) * c;
    q.y = p_dir.y * cos_t0 - cos_azi * sin_t0 * sin_t;
    q.z = p_dir.z * cos_t0 + ( p_dir.y * p_dir.z * cos_azi - p_dir.x * sin_azi ) * c;
  }
  p_dir.x = q.x;
  p_dir.y = q.y;
  p_dir.z = q.z;
}

// scatter Particle given input direction cosine cos_t0 = mu0
void Particle::free_gas_scatter( double cos_t0, double* A, double* T ) {
  // sample a random azimuthal angle uniformly
  double azi     = 2.0 * pi * Urand();
  double cos_azi = std::cos(azi);
  double sin_azi = std::sin(azi);
  double kt = k * *T;
  // double ThermalCutoffEnergy = 400 * kt;
  // bool therscat = false;
  double mu_c = cos_t0;
  
  point v_cm, p_dir_c, q;
  // point u, v_c;
  // double p_speed_c;
  // p_speed = std::sqrt( 2.0 * p_energy * conv_const / p_mass );
  double vel = std::sqrt( p_energy );
  point v_n = p_dir * vel;
  
  
  // point v_lab = p_dir * p_speed;
  // point v_lab = p_dir * v_vel;
  
  
  // if ( *T == 0.0 || p_energy > ThermalCutoffEnergy ) { // If not thermal scattering
  //   double E_cm    = std::pow( ( *A / ( *A + 1 ) ), 2) * p_energy; // Particle energy in center of mass (CM)
  //   double E_lab   = E_cm + ( ( p_energy + 2 * cos_t0 * ( *A + 1 ) * std::sqrt( p_energy * E_cm ) ) / std::pow( ( *A + 1 ), 2) ); // Particle energy in lab frame
  //   mu_c           = cos_t0 * std::sqrt( E_cm / E_lab ) + std::sqrt( p_energy / E_lab ) / ( *A + 1 ); // Get new mu_c
  //   p_energy       = E_lab; // Set new particle energy
  //   p_speed        = std::sqrt( 2 * p_energy * conv_const / p_mass ); // Set new particle speed
  //   p_dir_c        = p_dir;
  // } else {
    // // Perform thermal scattering procedure
  double awr = *A / p_amu;
  double beta_vn = std::sqrt( awr * p_energy / kt );
  double alpha = 1.0 / ( 1.0 + std::sqrt( pi ) * beta_vn / 2.0 );
  double beta_vt_sq, mu_tilde, accept_prob;
  do { // Sample target velocity and direction
    double r1 = Urand();
    double r2 = Urand();
    if ( Urand() < alpha ) {
      beta_vt_sq = - std::log( r1 * r2 );
    } else {
      double c = std::cos( Urand() * pi / 2.0 );
      beta_vt_sq = - std::log( r1 ) - std::log( r2 ) * c * c;
    }
    mu_tilde = 2 * Urand() - 1;
    double beta_vt = std::sqrt( beta_vt_sq );
    accept_prob = std::sqrt( beta_vn * beta_vn + beta_vt_sq - 2 * beta_vn * beta_vt * mu_tilde ) / ( beta_vn + beta_vt );
    
  } while ( Urand() > accept_prob );
  double vt = std::sqrt( beta_vt_sq * kt / awr );
  
  double a = std::sqrt( 1.0 - p_dir.z  * p_dir.z );
  double b = std::sqrt( 1 - mu_tilde * mu_tilde );
  double c = b / a;
  point v_t;
  if ( a > std::numeric_limits<double>::epsilon() * 1000.0 ) {
  v_t.x = mu_tilde * p_dir.x + ( p_dir.x * p_dir.z * cos_azi - p_dir.y * sin_azi ) * c;
  v_t.y = mu_tilde * p_dir.y + ( p_dir.y * p_dir.z * cos_azi + p_dir.x * sin_azi ) * c;
  v_t.z = mu_tilde * p_dir.z - cos_azi * a * b;
  } else {
  a = std::sqrt( 1.0 - p_dir.y  * p_dir.y );
  c = b / a;
  v_t.x = mu_tilde * p_dir.x + ( p_dir.x * p_dir.y * cos_azi + p_dir.z * sin_azi ) * c;
  v_t.y = mu_tilde * p_dir.y - cos_azi * a * b;
  v_t.z = mu_tilde * p_dir.z + ( p_dir.y * p_dir.z * cos_azi - p_dir.x * sin_azi ) * c;
  }
  v_t = v_t * vt;
  
  v_cm = ( v_n + v_t * awr ) / ( awr + 1.0 );
  v_n = v_n - v_cm;
  vel = v_n.magnitude();
  p_dir_c = v_n / vel;

  // rotate the local Particle coordinate system aligned along the incident direction
  // to the global problem (x,y,z) coordinate system 
  double sin_t0 = std::sqrt( 1.0 - mu_c * mu_c );
  double sin_t  = std::sqrt( 1.0 - p_dir_c.z  * p_dir_c.z );
  double c2 = sin_t0 / sin_t;

  if ( sin_t > std::numeric_limits<double>::epsilon() * 1000.0 ) {
    q.x = p_dir_c.x * mu_c + ( p_dir_c.x * p_dir_c.z * cos_azi - p_dir_c.y * sin_azi ) * c2;
    q.y = p_dir_c.y * mu_c + ( p_dir_c.y * p_dir_c.z * cos_azi + p_dir_c.x * sin_azi ) * c2;
    q.z = p_dir_c.z * mu_c - cos_azi * sin_t0 * sin_t;
  } else {
    // if incident direction along z, reorient axes to avoid division by zero
    sin_t  = std::sqrt( 1.0 -  p_dir_c.y * p_dir_c.y );
    c2 = sin_t0 / sin_t;
    q.x = p_dir_c.x * mu_c + ( p_dir_c.x * p_dir_c.y * cos_azi + p_dir_c.z * sin_azi ) * c2;
    q.y = p_dir_c.y * mu_c - cos_azi * sin_t0 * sin_t;
    q.z = p_dir_c.z * mu_c + ( p_dir_c.y * p_dir_c.z * cos_azi - p_dir_c.x * sin_azi ) * c2;
  }
  
  // if (therscat) {
  v_n      = q * vel;
  v_n      = v_n + v_cm;
  p_energy = v_n.magnitude() * v_n.magnitude();
  vel      = std::sqrt( p_energy );
  p_dir    = v_n / vel;
  p_speed  = std::sqrt( 2.0 * p_energy * conv_const / p_mass );
    
    // v_lab    = q * p_speed_c + u;
    // p_speed  = v_lab.magnitude();
    // p_energy = 0.5 * p_mass * p_speed * p_speed / conv_const;
    // // std::cout << p_energy << std::endl;
    // // p_dir    = v_lab / p_speed;
    // p_dir    = v_lab / p_speed;
    
  // } else {
  //   p_dir.x = q.x;
  //   p_dir.y = q.y;
  //   p_dir.z = q.z;
  // }
}

// set the Particles life flag to false
void Particle::kill() {
  exist = false;
}

// set the Particle's direction
void Particle::setDirection( point p ) {
  p_dir.x = p.x;
  p_dir.y = p.y;
  p_dir.z = p.z;
}

// adjust the weight by a factor f
void Particle::adjustWeight( double f ) {
  p_wgt *= f;
}

// set the cell pointer for efficiency
void Particle::recordCell( std::shared_ptr< Cell > cel ) {
  p_cell = cel;
}

double Particle::time_traveled( double d ) {
  return d / p_speed;
}

double Particle::dist_traveled( double t ) {
  return t * p_speed;
}
