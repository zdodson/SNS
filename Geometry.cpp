#include "Geometry.h"

#include <vector>
#include <memory>
#include <math.h>
#include "Random.h"

void Geometry::changeCell( Particle* p, std::stack<Particle>* bank ) {
  std::stack<Particle> bankWW;
  
  double I1 = p->cellPtr()->getImportance(); // get original cell importance
  for(auto e : p->cellPtr()->getEstimators()){ e->add_exit_energy(p); }
  // bool foundflag = determineCellBON( p );
  determineCell( p );
  // std::cout << CellList.size() << " " << p->cellPtr()->getNeighbors().size() << "\n";
  for(auto e : p->cellPtr()->getEstimators()){ e->add_entrant_energy(p); }
  double I2 = p->cellPtr()->getImportance(); // Get the importance of new cell
  double R = I2 / I1; // ratio of importances
  double intpart, n;
  
  if ( I2 == 0.0 ) {
    p->kill();
  } 
  else if ( R > 1.0 ) { // Split particle
    if ( std::modf( R, &intpart ) == 0.0 ) { // If R is an integer
      n = R;
    } 
    else { // R is not an integer
      n = std::floor( R + Urand() );
    }

    p->adjustWeight( 1.0 / n ); // Change weight accordingly
    for ( unsigned long long i = 1 ; i < n ; i++ ) { // it starts from one so that the current particle doesn't get pushed onto the stack. The current particle goes on.
      bankWW.push( *p ); // Add a particle to the bank.
    }
  }
  else if ( R < 1.0 ) { // Roulette particle
    if ( Urand() < R ) {
      p->adjustWeight( 1.0 / R );
    } 
    else {
      p->kill();
    }
  }
  
  //Weight Windows
  std::stack<Particle> bankFC;
  bankWW.swap( bankFC );
  
  //do weight windows for all particles in BankWW and current particle 
  if ((p->alive()) && (p->cellPtr()->getWeightWindow())) {
	
    double PWUB = p->cellPtr()->getUpperbound_weight(); //gets upperbound specified of the weight
  	double PWLB = p->cellPtr()->getLowerbound_weight(); //gets lowerbound specified of the weight
	
  	if(PWUB < PWLB) {
  	  std::cout<<"Dumbass, adjust window range, make sure Upperbound is greater then lowerbound"<<std::endl;
  	  throw;	 
  	}// upper bound Window must be at least 2 times great than lower bound for good results  
  	if((!PWUB) >= 2.0*PWLB) {
        std::cout<<"Dumbass, adjust window range, make sure upperbound is 2*lowerbound"<<std::endl;
  	  throw;	 	
  	}
	
    Particle weighted_p = *p;
	 
    int bankWWSize = bankWW.size();
    for (int i = 0; i < (bankWWSize + 1); i++) {
	 
	    //PARTICLE ABOVE WEIGHT UPPERBOUND 
      if (p->wgt() > PWUB) {
		    double Ratio_weight = p->wgt()/PWLB;
	      double split_factor = std::floor(Ratio_weight); //round down
	      p->adjustWeight(1.0/split_factor); //adjust weight of particle for max number of particles to fit in window 
	      for ( unsigned long long i = 0; i < split_factor*p->wgt(); i++ ) {
        bankFC.push( weighted_p); // Add a particle to the bank.
	      }
      } else if (p->wgt() < PWLB) { ////PARTICLE BELOW WEIGHT LOWERBOUND
		    //for probability to fit in the lower window 
		    double Probability = p->wgt()/PWLB;
	      if (Urand() < Probability) { //roulette particle 
		      p->adjustWeight(1.0/Probability);
		    } else {
		      p->kill();
        }
	    } else { //PARTICLE IN WINDOW 
	      // do nothing, particle is in window
	    }
	    // Set up the next set of particles 
  	  if (! bankWW.empty()) {
  		  weighted_p = bankWW.top();
  	    bankWW.pop();
  	    p->adjustWeight( 1.0);
  	  }
    }
  } else {
    int bankWWSize = bankWW.size();
  	for (int i = 0; i < bankWWSize; i++) {
  	  bankFC.push(bankWW.top());
      bankWW.pop();
  	}
  }
  
  
  if ((p->alive()) && (p->cellPtr()->getForcedCollision())) {
    // if (false)
    // do forced collisions
    // We will do forced collisions for all particles in the tempBank and the current particle.
    Particle transmitted_p = *p;
    Particle collided_p    = *p;
    p->kill();
    
    // std::cout << collided_p.alive() << "\n";
    // throw;
    
    // Get next surface
    std::pair< std::shared_ptr< Surface >, double > S = transmitted_p.cellPtr()->SurfaceIntersect( transmitted_p.getRay() ); 
    double sigma_total = transmitted_p.cellPtr()->getMaterial()->macro_xs(&transmitted_p);
    
    truncated_exponential_Distribution trunc_exp("forced collision trucated exp",sigma_total,S.second);
    
    int bankFCSize = bankFC.size();
    for (int i = 0; i < (bankFCSize + 1); i++) { // plus one for the particle that is still alive but not in the tempBank
      // Transmit first
      // Adjust the weight
      transmitted_p.adjustWeight(std::exp(-sigma_total*S.second));
      // Move the particle
      transmitted_p.cellPtr()->moveParticle( &transmitted_p, S.second, this->getActiveFlag() ); 
      // Cross the surface
      S.first->crossSurface( &transmitted_p , 0.0 , transmitted_p.cellPtr()->getExponentialTransform() , 
                              transmitted_p.cellPtr()->macro_xs( &transmitted_p ) , 
                              transmitted_p.cellPtr()->getTransformParameter() ); 
      // Change the cell
      this->changeCell( &transmitted_p, bank ); 
      // If it comes back alive, bank it
      if (transmitted_p.alive()) {
        bank->push(transmitted_p);
      }     
      
      // Collide
      // Adjust the weight
      collided_p.adjustWeight(1-std::exp(-sigma_total*S.second));
      // Move the particle
      collided_p.cellPtr()->moveParticle( &collided_p, trunc_exp.sample(), this->getActiveFlag() ); 
      // ?? It was in the simulation loop
      for ( auto m : this->MeshList ) { m->score(&collided_p); }
      // Sample collision
      collided_p.cellPtr()->sampleCollision( &collided_p, bank, this->get_TimeLimit(), this->getActiveFlag() , 0.0 ); 
      if (collided_p.alive()) {
        bank->push(collided_p);
      } 
      
      // Set up the next set of particles from the tempBank 
      if (! bankFC.empty()) {
        transmitted_p = bankFC.top();
        collided_p    = bankFC.top();
        bankFC.pop();
      }
    }
  }
  else {
    int bankFCSize = bankFC.size();
    for (int i = 0; i < bankFCSize; i++) {
      bank->push(bankFC.top());
      bankFC.pop();
    }
  }
}

// Sample source particle from source list
Particle Geometry::getSourceParticle() {
  double xi = Urand();
  int r;
  for ( int i = 0 ; i < src_cdf.size() ; i++ ) {
    if ( xi < src_cdf[i] ) {
      r = i;
      break;
    }
  }
  return SrcList[r]->sample( CellList );
}

// add source list to geometry
void Geometry::addSources( std::vector< std::shared_ptr<Source> > SList ) {
  N_src = SList.size();
  double sum = 0.0;
  for ( auto s : SList ) { // generate source cdf from source probabilities
    sum+=s->getProbability();
    src_cdf.push_back( sum );
  }
  if ( bool isEqual = std::fabs( sum - 1.0 ) <= 1e-5 ) { // check if probabilities add to one (or close)
    SrcList = SList;
  } else {
    std::cout << "Source emission probabilities do not sum to unity." << std::endl;
    throw;
  }
}

void Geometry::setType( std::string type ) {
  if (type == "Fixed Source" || type == "Eigenvalue") {
    run_type = type;
  } else {
    std::cout << "Error, run type " << type << " not supported/recognized." << std::endl;
    throw;
  }
}

void Geometry::printInfo() {
  std::cout << "Run Type        : " << run_type << std::endl;
  std::cout << "Case Name       : " << simulation_name << std::endl;
  if ( run_type == "Fixed Source" ) {
    std::cout << "Total Particles : " << N_particles << std::endl;
  } else if ( run_type == "Eigenvalue" ) {
    std::cout << "Batch Size      : " << N_particles << std::endl;
    std::cout << "Inactive cycles : " << inactiveCycles << std::endl;
    std::cout << "Active cycles   : " << activeCycles << std::endl;
    std::cout << "keff Guess      : " << keff_guess << std::endl;
  }
  std::cout << "\n" << "Running..." << std::endl;
}

void Geometry::determineCell( Particle* p ) {
  bool foundCell = false;
  for ( auto c : CellList ) { // determine new cell
    if ( c->testPoint( p->pos() ) ) {
      p->recordCell( c );
      foundCell = true;
    }
  }
  if ( ! foundCell ) { // If lost, report particle location
    std::cout << "Error, particle lost! Particle location:" << std::endl;
    std::cout << "x = " << p->pos().x << std::endl;
    std::cout << "y = " << p->pos().y << std::endl;
    std::cout << "z = " << p->pos().z << std::endl;
    throw;
  }
}

bool Geometry::determineCellBON( Particle* p ) {
  for ( auto c : p->cellPtr()->getNeighbors() ) {
    if ( c->testPoint( p->pos() ) ) {
      p->recordCell( c );
      return true;
    }
  }
  return false;
}

void Geometry::incrementEntropy( Particle* p, int n ) {
  shannon_mesh->scoreEntropy( p, n );
}

void Geometry::finalize() {
  Clock = std::clock() - Clock;
  if ( run_type == "Eigenvalue" ) { std::cout << std::endl << std::fixed << "Final average collision and track length k-eff = " << k_av[1] << " (" << k_av[2] << ")" << std::endl; }
  std::cout << "=> " << std::fixed << std::setprecision(0) << nhist/( (double)Clock/CLOCKS_PER_SEC ) << " particles/second" << std::endl << std::setprecision(4);
}

void Geometry::init_k_problem() {
  k_col[0] = keff_guess;  // initial guess, serves as current keff for scaling of fission production
  k_tl[0]  = keff_guess;
  start_clock();
}

void Geometry::reset_k_gen() {
  k_next.first  = 0.0;
  k_next.second = 0.0;
}

void Geometry::end_k_gen( unsigned long long i, int s ) {
  k_col[0] = k_next.first  / N_particles;
  k_tl[0]  = k_next.second / N_particles;
  
  addHistories( s );
  
  // compute the shannon entropy
  entropy     = 0.0;
  entropy_sum = 0.0;
  for ( unsigned j = 0 ; j < shannon_mesh->getSize() ; j++ ) { entropy_sum += shannon_mesh->getValue(j); }
  for ( unsigned j = 0 ; j < shannon_mesh->getSize() ; j++ ) {
    double pj = shannon_mesh->getValue(j) / ( entropy_sum );
    if ( pj > 0.0 ) { entropy -= pj * std::log( pj ); } 
  }
  
  // compute output and write information to screen
  if ( i > inactiveCycles ) {
    k_col[1] += k_col[0];
    k_col[2] += k_col[0] * k_col[0];
    
    k_tl[1] += k_tl[0];
    k_tl[2] += k_tl[0] * k_tl[0];
    
    if ( ! activeCycleFlag ) { ActivateHistories(); };
  }
  
  nactive = std::max( 1u, (unsigned)(i - inactiveCycles) );
  k_running.first = k_col[1] / nactive;
  k_running.second = k_tl[1] / nactive;
  
  k_err.first  = std::sqrt( ( k_col[2] / nactive - k_running.first  * k_running.first  ) / nactive );
  k_err.second = std::sqrt( ( k_tl[2]  / nactive - k_running.second * k_running.second ) / nactive );
  
  k_av[0] = ( k_col[0] + k_tl[0] ) / 2;
  k_av[1] = ( k_running.first + k_running.second ) / 2;
  k_av[2] = ( k_err.first + k_err.second ) / 2;
  
  if ( i == 1 ) { std::cout << std::setw(5) << "cycle" << "         " << "k" << "         " << "H"
    << "   " << "   kav" << "   " << "  std dev" << std::endl; };
  
  
  std::cout << std::setw(5) << i << "   " <<
    std::fixed << std::setw(7) << std::setprecision(5) << k_av[0] << "  " <<
    std::setw(7) << std::scientific << std::setprecision(2) << entropy << std::fixed << "   ";
  if ( activeCycleFlag ) {
    std::cout << std::setw(7) << std::setprecision(5) << k_av[1] << "   ";
    std::cout << std::setw(7) << std::setprecision(5) << k_av[2] << std::endl;
  } else {
    std::cout << std::endl;
  }
  
  if ( i == inactiveCycles ) { std::cout << "   ----- Begin active cycles ----- " << std::endl; };
}

void Geometry::add_to_fission_bank( double wgt, Particle* p, std::stack<Particle>* fission_bank ) {
  neutrons = std::floor( wgt * p->cellPtr()->getMaterial()->getf(p) / k_col[0] + Urand() );
  
  incrementEntropy( p, neutrons );
  for ( unsigned n = 0 ; n < neutrons ; n++ ) {
    Particle SourceParticle = Particle( p->pos(), isotropic->sample(), p->cellPtr()->getMaterial()->sampleFissionEnergy( p ), 0.0 );
    SourceParticle.recordCell( p->cellPtr() );
    fission_bank->push( SourceParticle );
  }
}


