#include <string>
#include <cmath>

#include "Distribution.h"
#include "Point.h"
#include "Random.h"

// Sample uniformly b/w two points
double uniform_Distribution::sample() { return a + Urand() * ( b - a ); }

// Sample along a linear function with arbitrary bounds
double linear_Distribution::sample() {
  double r1 = Urand(), r2 = Urand();
  double p  = 2.0 * std::fmin( fa, fb ) / ( fa + fb );
  if ( r1 < p ) { return a + r2 * ( b - a ); }
  else {
    if ( fb > fa ) { return a + ( b - a ) * std::sqrt( r2 ); }
    else           { return a + ( b - a ) * ( 1.0 - std::sqrt( r2 )); }
  }
}

// Distribution used to sample distance to collision
double exponential_Distribution::sample() { return -std::log( Urand() ) / lambda; }

truncated_exponential_Distribution::truncated_exponential_Distribution( std::string label, double l, double s ) : Distribution(label), lambda(l)
{
  term1 = 1 - std::exp(-lambda*s);
}

double truncated_exponential_Distribution::sample()
{
  return -std::log(1 - Urand()*term1)/lambda;
}

// Not used right now
double normal_Distribution::sample() 
{ return mu + sigma * std::sqrt( -2.0 * std::log( Urand() ) ) * std::cos( twopi * Urand() ); }

// Sample Heyney-Greenstein distribution for scattering
double HenyeyGreenstein_Distribution::sample() {
  if ( a != 0.0 ) {
    return ( 1.0 + a*a - pow( ( 1.0 - a*a )/( 1.0 + a*(2.0*Urand() - 1.0) ), 2 ) ) / ( 2.0 * a ); 
  }
  else {
    return 2.0 * Urand() - 1.0;
  }
}

// Average multiplicity dist. for fission
int meanMultiplicity_Distribution::sample() {
  return (int) std::floor( nubar + Urand() );
}

// Terrell fission dist.
TerrellFission_Distribution::TerrellFission_Distribution( std::string label, double p1, double p2, double p3 ) 
    : Distribution(label), sigma(p1), b(p2), nu_bar(p3) {
  double c  = 0.0;
  double nu = 0.0;
  while ( c < 1.0 - 1.0e-12 ) {
    double a  = ( nu - nu_bar + 0.5 + b ) / sigma;
    c = 0.5 * ( 1 + erf( a / sqrt(2.0) ) ) ;

    cdf.push_back(c);
    nu += 1.0;
  }
  cdf.push_back(1.0);
}

int TerrellFission_Distribution::sample() {
  double r  = Urand();
  double nu;
  for ( int i = 0 ; i < cdf.size() ; i++ ) {
    if ( r < cdf[i] ) {
      nu = (double) i;
      break;
    }
  }
 return nu; 
}

// Sample isotropic direction
point isotropicDirection_Distribution::sample() {
  // sample polar cosine and azimuthal angle uniformly
  double mu  = 2.0 * Urand() - 1.0;
  double azi = twopi * Urand();

  // convert to Cartesian coordinates
  double c = std::sqrt( 1.0 - mu * mu );
  point p;
  p.x = std::cos( azi ) * c;
  p.y = std::sin( azi ) * c;
  p.z = mu;

  return p;
}

// Sample anisotropic direction
point anisotropicDirection_Distribution::sample() {
  double mu  = dist_mu->sample(); 
  double azi = twopi * Urand();
  double cos_azi = std::cos(azi);
  double sin_azi = std::sin(azi);

  // rotate the local particle coordinate system aligned along the incident direction
  // to the global problem (x,y,z) coordinate system 
  double sin_t0 = std::sqrt( 1.0 - mu * mu );
  double c = sin_t0 / sin_t;

  point p;
  p.x = axis.x * mu + ( axis.x * axis.z * cos_azi - axis.y * sin_azi ) * c;
  p.y = axis.y * mu + ( axis.y * axis.z * cos_azi + axis.x * sin_azi ) * c;
  p.z = axis.z * mu - cos_azi * sin_t0 * sin_t;
  return p;
}

// sample in xyz box
point independentXYZ_Distribution::sample() {
  return point( dist_x->sample(), dist_y->sample(), dist_z->sample() );
}

// Sample in spherical dist.
point uniformSPH_Distribution::sample() {
  // Sample coordinates based on inputs
  double rcu = dist_rsq->sample();
  double mu  = dist_mu->sample();
  double phi = dist_phi->sample();
  
  double r = std::pow( rcu, 1.0 / 3.0 ); // Since r is sampled in r^3, take cube root
  double theta = std::acos( mu ); // Since mu is sampled, take acos to get theta
  
  // Convert back to Cartesian
  double x = r * std::sin( theta ) * std::cos( phi ) + location.x;
  double y = r * std::sin( theta ) * std::sin( phi ) + location.y;
  double z = r * std::cos( theta ) + location.z;

  return point( x , y , z );
}

// Sample in cylindrical dist.
point uniformCYL_Distribution::sample() {
  // Sample coordinates based on inputs
  double rsq   = dist_rsq->sample();
  double theta = dist_theta->sample();
  double z     = dist_z->sample();
  
  double r = std::sqrt( rsq ); // Since r is sampled in r^2, take square root
  
  // Convert back to Cartesian
  double x = r * std::cos( theta ) + location.x;
  double y = r * std::sin( theta ) + location.y;
  z += location.z;

  double final_x = x;
  double final_y = y;
  double final_z = z;

  // Rotate around x-axis
  if ( thetaX != 0.0 ) {
    final_y = y * std::cos( thetaX ) - z * std::sin( thetaX );
    final_z = y * std::sin( thetaX ) + z * std::cos( thetaX );
  }

  // Rotate around y-axis
  if ( thetaY != 0.0 ) {
    final_x =  x * std::cos( thetaY ) + z * std::sin( thetaY );
    final_z = -x * std::sin( thetaY ) + z * std::cos( thetaY );
  }

  // Rotate around z-axis
  if ( thetaZ != 0.0 ) {
    final_x = x * std::cos( thetaZ ) - y * std::sin( thetaZ );
    final_y = x * std::sin( thetaZ ) + y * std::cos( thetaZ );
  }
  
  return point( final_x , final_y , final_z );
}

double Problem6_Distribution::sample() {
  double x, y, f;
  double c1 = 1.0 / a;
  do {
    x = 2.0 * Urand() - 1.0;
    y = 1.501 * Urand();
    f = c1 * ( 1.0 + std::pow( x + 1, 3 ) );
  } while ( y > f );
  return x;
}

double WattFissionSpectrum::sample() {
  double x, y;
  do {
  x = - std::log( Urand() );
  y = - std::log( Urand() );
  } while ( std::pow( y - M * ( x + 1 ), 2 ) > b * L * x );
  return L * x;
}
