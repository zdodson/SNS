#include "Reaction.h"

reaction::reaction( double a, double b, std::string xs_filename, int ind, std::shared_ptr<FBotfdata> FB, bool disab ) : const_xs(a), energy_dependent_xs(b), cross_section_filename(xs_filename), otf_ix(ind), otf_data_ptr(FB), otf_disabled(disab)
{
	// read data for cross section in the constructor
	// "blank" file refers to an energy dependent cross section that is dependent on the analytical expression
	// xs = a + b / sqrt(E) where E is in MeV.
	if (xs_filename == "blank")
	{
		is_tabular_data = false;
	} 
	// data file is provided with tabulated cross sections
	else {
		is_tabular_data = true;
		std::ifstream xs_file;
		std::string temp_line;
		double temp_energy;
		double temp_xs;
    
		xs_file.open("XS_DATA/" + xs_filename);
    if ( ! xs_file ) { std::cout << "Error, could not open cross section file " << xs_filename << "." << std::endl; throw; }

		for (int i = 0; i < 3; i++) // need to skip the first three line which are useless data for us.
		{
			std::getline(xs_file, temp_line, '\n');
		}
    
		while (!xs_file.eof())
		{
			xs_file >> temp_energy >> temp_line >> temp_xs;
			energy_table.push_back(temp_energy);
			xs_table.push_back(temp_xs);
		}
		energy_table.pop_back(); // We pop back the last element because the reading in portion pushes back on extra time
		xs_table.pop_back();
	 
    xs_file.close();
		//convert to MeV
		for (int i = 0; i < energy_table.size(); i++){
			energy_table.at(i) = energy_table.at(i) / 1000000.0;
		}
	}
}

void  capture_reaction::sample( Particle* p, std::stack< Particle >* bank ) {
  // kill the particle and leave the bank unmodified
  p->kill();
}

void  scatter_reaction::sample( Particle* p, std::stack< Particle >* bank ) {
  // scatter the particle and leave the bank unmodified
  double mu0 = scatter_dist->sample();
  p->scatter( mu0 );
}

void  free_gas_scatter_reaction::sample( Particle* p, std::stack< Particle >* bank ) {
  // scatter the particle and leave the bank unmodified
  double mu0 = scatter_dist->sample();
  // double ini_en = p->energy();
  p->free_gas_scatter( mu0, A, MatTempPtr );
  // double fin_en = p->energy();
  // std::cout << std::scientific << ini_en << "  " << ini_en-fin_en << "\n";
}

void  fission_reaction::sample( Particle* p, std::stack< Particle >* bank ) {
  // create random number of secondaries from multiplicity distribution and
  // push all but one of them into the bank, and set working particle to the last one
  // if no secondaries, kill the particle
  if ( problem_type == "Fixed Source" ) {
    int n = multiplicity_dist->sample();
    if ( n <= 0 ) {
      p->kill();
    }
    else {
      // bank all but last particle (skips if n = 1)
      for ( int i = 0 ; i < (n - 1) ; i++ ) {
        Particle q( p->pos(), isotropic->sample(), sampleFissionE(), 0.0 );
        q.recordCell( p->cellPtr() );
        bank->push( q );
      }
      // set working particle to last one
      Particle q( p->pos(), isotropic->sample(), p->energy(), p->getTime() );
      q.recordCell( p->cellPtr() );
      *p = q;
    }
  } else if ( problem_type == "Eigenvalue" ) {
    p->kill();
  }
}
