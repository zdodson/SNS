#ifndef _SURFACE_HEADER_
#define _SURFACE_HEADER_

#include <string>
#include <vector>
#include <limits>

#include "Point.h"
#include "Particle.h"
#include "Estimator.h"

class Surface {
  private:
    std::string surface_name;
    bool        reflect_bc;
    std::vector< std::shared_ptr< Estimator > > surface_estimators;
  public:
     Surface( std::string label ) : surface_name(label) { reflect_bc = false; };
    ~Surface() {};

    virtual std::string name()    final { return surface_name; };
    virtual void makeReflecting() final { reflect_bc = true; };

    virtual void attachEstimator( std::shared_ptr< Estimator > E ) final {
      surface_estimators.push_back( E );
    }
    virtual void scoreEstimators( Particle* p ) final {
      for ( auto e : surface_estimators ) { e->score( p ); }
    }


    virtual void crossSurface( Particle* p , double s , bool ET , double macXS , double a) final { 
      // check if Exponential Transform (ET)
      if ( ET ) {
        double mu      = std::atan( p->dir().y / p->dir().x );
        double wb      = std::exp( -a*macXS*mu*s );
        p->adjustWeight( wb );
      }

      // score estimators
      for ( auto e : surface_estimators ) { e->score( p ); }
      // reflect if needed
      if ( reflect_bc ) { 
        point d = reflect( p->getRay() );
        p->setDirection( d );
      }
      // advance particle off the surface
      p->move( std::numeric_limits<float>::epsilon() );
    }

    virtual double eval( point p )   = 0;
    virtual double distance( ray r ) = 0;
    virtual point  reflect( ray r )  = 0;
};

class plane : public Surface {
  private:
    double a, b, c, d;
  public:
     plane( std::string label, double p1, double p2, double p3, double p4 ) : 
       Surface(label), a(p1), b(p2), c(p3), d(p4) {};
    ~plane() {};

     double eval( point p );
     double distance( ray r );
     point  reflect( ray r );
};

class sphere : public Surface {
  private:
    double x0, y0, z0, rad;
  public:
     sphere( std::string label, double p1, double p2, double p3, double p4 ) : 
       Surface(label), x0(p1), y0(p2), z0(p3), rad(p4) {};
    ~sphere() {};

     double eval( point p );
     double distance( ray r );
     point  reflect( ray r );
};

class x_cylinder : public Surface {
  private:
    double y0, z0, rad;
  public:
     x_cylinder( std::string label, double p1, double p2, double p3 ) : 
       Surface(label), y0(p1), z0(p2), rad(p3) {};
    ~x_cylinder() {};

     double eval( point p );
     double distance( ray r );
     point  reflect( ray r );
};

class z_cylinder : public Surface {
  private:
    double x0, y0, rad;
  public:
     z_cylinder( std::string label, double p1, double p2, double p3 ) : 
       Surface(label), x0(p1), y0(p2), rad(p3) {};
    ~z_cylinder() {};

     double eval( point p );
     double distance( ray r );
     point  reflect( ray r );
};

class x_cone : public Surface {
  private:
    double x0, y0, z0, rad;
  public:
     x_cone( std::string label, double p1, double p2, double p3, double p4 ) : 
       Surface(label), x0(p1), y0(p2), z0(p3), rad(p4) {};
    ~x_cone() {};

     double eval( point p );
     double distance( ray r );
     point  reflect( ray r );
};

#endif
