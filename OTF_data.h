#ifndef _FBOTFDATA_HEADER_
#define _FBOTFDATA_HEADER_

#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>
#include <string>
#include <memory>

class FBotfdata {
  private:
    std::string otf_name;
    std::string otf_file;
    bool otf_disabled;

    int					zaid;
    int 				nuclide_index; // unused?
    std::string				version;
    std::string				date;
    std::string				info1;
    std::string				info2;
    double				tmin;		//min temp, Kelvin
    double				tmax;		//max temp, Kelvin
    double 				toffset;	//for scaling, (tmax-tmin)/50
    double				tscale1;	//for scaling, 1/(tmax-tmin+toffset)
    double				tscale2;	//for scaling, tmin-toffset
    double				emin;		//min energy, MeV
    double				emax;		//max energy, MeV
    int					ordermax;	//=(ncoefmax-1)/2
    int					ncoefmax;	//max number of coefs for (mt, ie)
    int					ncoeftot;	//total size of coef array
    int					ne;		//number of energy points
    int					nmt;		//number of reactions
    int					mtfis;		//MT number for total fission (18 or 19)
    int					ixfis;		//index in MT array for fission
    int					ix1;		//index in MT array for total
    int					ix2;		//index in MT array for elastic
    int					ix101;		//index in MT array for disappearance
    int					ix202;		//index in MT array for gamma prod
    int					ix301;		//index in MT array for heating
    std::vector<int>			mt;		//mt numbers for fits
    std::vector<double>  		otf_e;		//energies
    std::vector<std::vector<double> > 	lcoef;		//dim=[ne][nmt+1]
    std::vector<double>			coef;		//dim=ncoeftot
    std::vector<double> 		nu;		//nu for interp of nu_fission
    int					otf_ncoef_max = 21;
    
  public:
    FBotfdata( std::string file, bool flag );
   ~FBotfdata() {};
    double calc_xs( int, double, double); // inputs needed: integer for reaction, energy, temperature
    double get_emax() { return emax; };
    double get_emin() { return emin; };
    double get_tmax() { return tmax; };
    double get_tmin() { return tmin; };

};

#endif
