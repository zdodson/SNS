#include "OTF_data.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Utility.h"

FBotfdata::FBotfdata( std::string file, bool flag ) :  otf_file(file), otf_disabled(flag)
{// need to fill FBotfdata from otf_file
  if (otf_disabled == false ) {
    std::ifstream otf_file_stream;
    std::string otf_temp_line;
    int temp_int;
    double temp_double;
    char temp_char;
    std::vector<double> temp_full_vec;
    temp_full_vec.clear();
    std::vector<double> temp_part_vec;

    otf_file_stream.open( otf_file.c_str() );// file opened: need to process input with otf read routine

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads zaid
    otf_temp_line.erase(0, 10);
    zaid = atoi(otf_temp_line.c_str() );

    // skip the next 4 lines: unneeded information
    for (int i=0; i<4; i++ ) { std::getline(otf_file_stream, otf_temp_line, '\n'); }

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads tmin
    otf_temp_line.erase(0, 10);
    tmin = atof(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads tmax
    otf_temp_line.erase(0,10);
    tmax = atof(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads emin
    otf_temp_line.erase(0,10);
    emin = atof(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads emax
    otf_temp_line.erase(0,10);
    emax = atof(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads order coef max
    otf_temp_line.erase(0,10);
    ordermax = atoi(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads ncoefmax
    otf_temp_line.erase(0,10);
    ncoefmax = atoi(otf_temp_line.c_str() );
 
    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads ncoeftot
    otf_temp_line.erase(0,10);
    ncoeftot = atoi(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads ne
    otf_temp_line.erase(0,10);
    ne = atoi(otf_temp_line.c_str() );
 
    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads nmt
    otf_temp_line.erase(0,10);
    nmt = atoi(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads mtfis
    otf_temp_line.erase(0,10);
    mtfis = atoi(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // reads ixfis
    otf_temp_line.erase(0,10);
    ixfis = atoi(otf_temp_line.c_str() );

    std::getline(otf_file_stream, otf_temp_line, '\n'); // read mt value vector
    otf_temp_line.erase(0,10);
    std::stringstream ss(otf_temp_line);
    while (ss >> temp_int)  mt.push_back(temp_int);

    for (int j=0; j<nmt; j++) {
      switch(mt[j]) {
        case 1:   {ix1 = j;
                   break;}
        case 2:   {ix2 = j;
                   break;}
        case 101: {ix101 = j;
                   break;}
        case 18:  {ixfis = j;
                   break;}
      }
    }
  
    std::getline(otf_file_stream, otf_temp_line, '\n'); // skip line
 
    for (int i=0; i<std::floor((ne+3)/4); i++ ) // read in energies
    {
      std::getline(otf_file_stream, otf_temp_line, '\n');
      std::stringstream stm(otf_temp_line);
      while ( stm >> temp_double ) otf_e.push_back(temp_double) ;
    }

    std::getline(otf_file_stream, otf_temp_line, '\n'); // skip line
    
    for (int i=0; i<std::floor(((nmt+1)*ne+7)/8); i++ ) // read in temp lcoef
    { 
      std::getline(otf_file_stream, otf_temp_line, '\n');
      std::stringstream lcstm(otf_temp_line);
      while ( lcstm >> temp_int )   temp_full_vec.push_back(temp_int);
    }

    for (int i{}, j{}; i != temp_full_vec.size(); ++i, ++j )
    {
      temp_part_vec.emplace_back(temp_full_vec[i]);
      if ( j==nmt ) 
      {
        lcoef.emplace_back(temp_part_vec);
        temp_part_vec.clear();
        j=-1;
      }
    }

    std::getline(otf_file_stream, otf_temp_line, '\n'); // skip line
   
    for (int i=0; i<std::floor((ncoeftot+3)/4); i++ ) // read in coef
    {
      std::getline(otf_file_stream, otf_temp_line, '\n');
      std::istringstream stm(otf_temp_line);
      while ( stm >> temp_double ) coef.push_back(temp_double) ;
    }

    toffset = (tmax - tmin ) / 50.0;
    tscale1 = 1.0/(tmax - tmin + toffset );
    tscale2 = tmin - toffset;
  }
}

double FBotfdata::calc_xs( int MTindex, double energy, double temperature) {
  double otf_xsec, otfs, otfr, sig1, sig2, otff;
  int ixmt, lc1, lc2, nc1, nc2, nc, bot, top, mid;
  std::vector<double> otfx(ncoefmax);
  switch(MTindex) {
    case 1   :{ ixmt = ix1;
		break;}
    case 2   :{ ixmt = ix2;
		break;}
    case 101 :{ ixmt = ix101;
		break;}
    case 18  :{ ixmt = ixfis;
		break;}
    case 19 :{ ixmt = ixfis;
               break;}
  }
  int otfk	= bin_search(&otf_e, energy);
  otff	= (energy-otf_e[otfk-1])/(otf_e[otfk]-otf_e[otfk-1]);
  lc1		= lcoef[otfk-1][ixmt];
  lc2		= lcoef[otfk][ixmt];
  nc1		= lcoef[otfk-1][ixmt+1] - lc1;
  nc2		= lcoef[otfk][ixmt+1]   - lc2;
  nc		= std::max(nc1, nc2);
  otfs		= std::sqrt(tscale1*(temperature-tscale2) );
  otfr		= 1.0;
  otfx[0]	= 1.0;
  for ( int i=2; i<=nc; i+=2 ) {
    otfr = otfr * otfs;
    otfx[i-1] = 1.0/otfr;
    otfx[i]   = otfr;
  }
  sig1 = 0.0;
  sig2 = 0.0;
  for (int i=0; i<nc1; i++) {
    sig1 += otfx[i]*coef[lc1 + i - 1];
  }
  for (int i=0; i<nc2; i++) {
    sig2 += otfx[i]*coef[lc2 + i - 1];
  }
  otf_xsec = (1.0-otff)*sig1 + otff*sig2;
//  std::cout << "otf xsec = " << otf_xsec << std::endl;
  return otf_xsec;
}
