#ifndef _POINT_HEADER_
#define _POINT_HEADER_

// point in 3d space
class point {
  public:
    double x, y, z;

     point( double a = 0.0, double b = 0.0, double c = 0.0 ) : x(a), y(b), z(c) {};
    ~point() {};

    void normalize();
    double magnitude();
    
    // Overload + operator to add two point objects.
    point operator+(const point& p) {
       point q;
       q.x = this->x + p.x;
       q.y = this->y + p.y;
       q.z = this->z + p.z;
       return q;
    }
    
    // Overload - operator to subtract two point objects.
    point operator-(const point& p) {
       point q;
       q.x = this->x - p.x;
       q.y = this->y - p.y;
       q.z = this->z - p.z;
       return q;
    }
    
    // Overload * operator to multiply point and double.
    point operator*(const double& a) {
       point q;
       q.x = this->x * a;
       q.y = this->y * a;
       q.z = this->z * a;
       return q;
    }
    
    // Overload / operator to divide point and double.
    point operator/(const double& a) {
       point q;
       q.x = this->x / a;
       q.y = this->y / a;
       q.z = this->z / a;
       return q;
    }
};

// a ray in 3d space; has a point of origin and a direction unit vector
class ray {
  public:
    ray( point p, point d );
    ~ray() {};

    point pos;
    point dir;
};

#endif