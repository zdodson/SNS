#ifndef _MATERIAL_HEADER_
#define _MATERIAL_HEADER_

#include <vector>
#include <string>
#include <utility>
#include <memory>

#include "Nuclide.h"
#include "Particle.h"

class Material {
  private:
    std::string Material_name;
    double      Material_atom_density;
    double      Temperature = 0.0;
    std::vector< std::pair< std::shared_ptr< Nuclide >, double > > Nuclides;
    std::vector< std::pair< std::shared_ptr< Nuclide >, double > > FissionableNuclides;
    double micro_xs(Particle* p);
  public:
     Material( std::string label, double aden ) : Material_name(label), Material_atom_density(aden) {};
    ~Material() {};

    std::string name() { return Material_name; }
    double atom_density() { return Material_atom_density; }
    std::vector< std::pair< std::shared_ptr< Nuclide >, double > > getNuclides() { return Nuclides; };

    void              addNuclide( std::shared_ptr< Nuclide >, double );
    void   addFissionableNuclide( std::shared_ptr< Nuclide >, double );
    void                 setTemp( double t ) { Temperature = t; };
    double              macro_xs( Particle* p );
    double          macro_abs_xs( Particle* p );
    double         macro_scat_xs( Particle* p );
    double         macro_fiss_xs( Particle* p );
    double         getReactionXS( Particle*, std::string );
    double                  getf( Particle* p ) { return getNuSigf( p ) / macro_xs( p ); };
    double             getNuSigf( Particle* p );
    double   sampleFissionEnergy( Particle* p );

    std::shared_ptr< Nuclide > sample_Nuclide(Particle* p);

    void sample_collision( Particle* p, std::stack<Particle>* bank );
};


#endif
