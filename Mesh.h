#ifndef _MESH_HEADER_
#define _MESH_HEADER_

#include <vector>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <utility>

#include "Cell.h"

// Tally to hold data for mesh
class Tally {
  private:

  public:
    // regular tally parameters
    std::string name;
    std::vector< double > tally_hist;
    std::vector< double > tally_sum;
    std::vector< double > tally_squared;
    double multiplier;
    std::string rxn_type;
    Tally( std::string tally_name, std::string tally_rxn, double mult ) : name(tally_name), rxn_type(tally_rxn), multiplier(mult) {};
    ~Tally() {};
    
    // initialize with n bins
    void init( int n ) {
      for ( unsigned long long i = 0 ; i < n ; i++ ) {
        tally_hist.push_back( 0.0 );
        tally_sum.push_back( 0.0 );
        tally_squared.push_back( 0.0 );
      }
    }
    
    // end history routine
    void endHistory() {
      for ( unsigned long long i = 0 ; i < tally_hist.size() ; i++ ) {
        double a          = tally_hist[i];
        tally_sum[i]     += a;
        tally_squared[i] += a * a;
        tally_hist[i]     = 0.0;
      }
    }
    
    double getMultiplier() { return multiplier; }
    std::string getTallyRxn() { return rxn_type; }
    std::string getTallyName() { return name; }
    double getHistValue( int i ) { return tally_hist[i]; };
};

// Mesh that holds tally and geometry data
class Mesh {
  private:

  protected:
    std::string file_name; // name
    std::vector<double> origin_point; // lower x, y, and z corner
    std::vector<int> spatial_dimensions; // number of bins in each direction
    std::vector<double> spatial_bin_widths; // Size of each bin
    std::vector<double> end_point; // upper x, y, and z corner

    std::vector < std::shared_ptr < Tally > > TallyList;
    
    int number_of_elements;
    std::string output_type; // type of output file
    unsigned long long nhist = 0;
  public:
    Mesh( std::string fname, std::vector<double> origin,
      std::vector<int> intervals, std::vector < std::shared_ptr < Tally > > TL ) : file_name(fname), origin_point(origin), spatial_dimensions(intervals), TallyList(TL) {};
    ~Mesh() {};
    
    virtual bool in_mesh( Particle* p ) = 0;
    virtual int lookup_bin( Particle* p ) = 0;
    virtual std::pair< std::vector< std::vector< double > >, std::vector<double> > get_points( int ) = 0;
    virtual void writeRawData( std::ofstream*, int, int ) = 0;
    virtual double getElementVolume( int ) = 0;
    std::vector < std::shared_ptr < Tally > > getTallyList() { return TallyList; };
    void    setNHistories( unsigned long long i ) { nhist = i; };
    void writeMesh( std::ofstream* );
    void writeConnectivity( std::ofstream* );
    void writeOffsets( std::ofstream* );
    void writeTypes( std::ofstream* );
    void writeTallies( std::ofstream*, int, int );
    void writeErrors( std::ofstream*, int, int );
    void closeFile( std::ofstream* );
    void writeVTUFile( int, int, int );
    void writeRawFile( int, int, int );
    // score if particle is in mesh
    void score( Particle* p ) {
      if ( in_mesh(p) ) {
        int bin = lookup_bin( p );
        for ( auto t : TallyList ) {
          double multiplier;
          if ( t->getTallyRxn() != "None" ) {
            multiplier = t->getMultiplier() * p->cellPtr()->getMaterial()->getReactionXS( p, t->getTallyRxn() ) / getElementVolume( bin );
          } else {
            multiplier = t->getMultiplier() * 1.0 / getElementVolume( bin );
          }
          double value = ( multiplier * p->wgt() / p->cellPtr()->getMaterial()->macro_xs(p) );
          t->tally_hist[ bin ] += value;
        }
      }
    }
    void report( int, int, int );
    
    // get points from x, y, and z bounds
    std::vector < std::vector < double > > cartesian_product( std::vector < std::vector < double > > input_vector ) {
      std::vector < std::vector < double > > result;
      
      for ( auto x : input_vector[0] ) {
        for ( auto y : input_vector[1] ) {
          for ( auto z : input_vector[2] )
            result.push_back( { x, y, z } );
        }
      }
      return result;
    }
    
    void endHistory() {
      nhist++;
      for ( auto t : TallyList ) {
        t->endHistory();
      }
    }
    
    void setOutputFormat( std::string type ) { output_type = type; };
};

class CartesianMesh : public Mesh {
  protected:
    double element_volume;
  public:    
    CartesianMesh( std::string fname, std::vector<double> origin,
                   std::vector<double> extent,
                   std::vector<int> intervals, std::vector < std::shared_ptr < Tally > > TL ) : Mesh(fname, origin, intervals, TL) {
      
      end_point.push_back( origin[0] + extent[0] );
      end_point.push_back( origin[1] + extent[1] );
      end_point.push_back( origin[2] + extent[2] );
      
      spatial_bin_widths.push_back( (end_point[0] - origin_point[0]) / intervals[0] );
      spatial_bin_widths.push_back( (end_point[1] - origin_point[1]) / intervals[1] );
      spatial_bin_widths.push_back( (end_point[2] - origin_point[2]) / intervals[2] );
      
      element_volume = spatial_bin_widths[0] * spatial_bin_widths[1] * spatial_bin_widths[2];
      number_of_elements = intervals[0] * intervals[1] * intervals[2];
      
      for ( auto t : TallyList ) { t->init( number_of_elements ); }
    }
    ~CartesianMesh() {};
  
  bool in_mesh( Particle* p );
  int lookup_bin( Particle* p );
  std::pair< std::vector< std::vector< double > >, std::vector<double> > get_points( int );
  void writeRawData( std::ofstream*, int, int );
  double getElementVolume( int bin_number ) { return element_volume; };
};

class CylindricalMesh : public Mesh {
  protected:
    const double pi    = std::atan(1.0)*4.0;
    const double twopi = 2.0*std::atan(1.0)*4.0;
    std::vector<double> element_volumes;
    std::vector<double> extent;
    // vector<vector<vector<double> > > array3D;
  public:
    CylindricalMesh( std::string fname, std::vector<double> origin,
                   std::vector<double> ext,
                   std::vector<int> intervals, std::vector < std::shared_ptr < Tally > > TL ) : Mesh(fname, origin, intervals, TL), extent(ext) {
      
      spatial_bin_widths.push_back( extent[0] / intervals[0] );
      spatial_bin_widths.push_back( extent[1] / intervals[1] );
      spatial_bin_widths.push_back( twopi / intervals[2] );
      
      number_of_elements = intervals[0] * intervals[1] * intervals[2];
      for ( int i = 0 ; i < number_of_elements ; i++ ) { element_volumes.push_back( 0.0 ); }
      
      for ( auto t : TallyList ) { t->init( number_of_elements ); }
    }
    ~CylindricalMesh() {};
  
  bool in_mesh( Particle* p );
  int lookup_bin( Particle* p );
  std::pair< std::vector< std::vector< double > >, std::vector<double> > get_points( int );
  void writeRawData( std::ofstream*, int, int );
  double getElementVolume( int bin_number ) { return element_volumes[ bin_number ]; }
  std::vector<double> convertPtToCartesian( std::vector<double> pt ) {
    return { pt[0] * std::cos( pt[2] ), pt[0] * std::sin( pt[2] ), pt[1] };
  }
  std::vector<double> convertPtToCylindrical( std::vector<double> pt ) {
    double theta = std::atan( pt[1] / pt[0] );
    if ( theta < 1.0e-13 ) { theta = 0; }
    return { std::sqrt( pt[0] * pt[0] + pt[1] * pt[1] ), pt[2], theta };
  }
};

class ShannonMesh : public CartesianMesh {
  protected:
    std::vector< double > entropy;
  public:
    ShannonMesh( std::string fname, std::vector<double> origin, std::vector<double> extent, std::vector<int> intervals, std::vector < std::shared_ptr < Tally > > TL ) : CartesianMesh( fname, origin, extent, intervals, TL ) {
      
      end_point.push_back( origin[0] + extent[0] );
      end_point.push_back( origin[1] + extent[1] );
      end_point.push_back( origin[2] + extent[2] );
      
      spatial_bin_widths.push_back( (end_point[0] - origin_point[0]) / intervals[0] );
      spatial_bin_widths.push_back( (end_point[1] - origin_point[1]) / intervals[1] );
      spatial_bin_widths.push_back( (end_point[2] - origin_point[2]) / intervals[2] );
      
      number_of_elements = intervals[0] * intervals[1] * intervals[2];
      
      for ( int i = 0 ; i < number_of_elements ; i++ ) { entropy.push_back( 0.0 ); }
    }
    ~ShannonMesh() {};
  
  double getSize() { return number_of_elements; };
  double getValue( int j ) { return entropy[j]; };
  void scoreEntropy( Particle* p, int n ) {
    if ( in_mesh( p ) ) {
      int bin = lookup_bin( p );
      entropy[ bin ] += n;
    }
  }
};

#endif