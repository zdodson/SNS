#include <cmath>
#include <iostream>
#include <iomanip>

#include "Estimator.h"
#include "Material.h"
#include "Cell.h"
#include "Particle.h"

void surface_current_estimator::score( Particle* p ) { tally_hist += p->wgt(); }

void counting_estimator::score( Particle* p ) { count_hist++; }

void track_length_estimator::score( Particle* p ) {}

void collision_estimator::score( Particle* p ) {
  if ( rxn_type != "None" ) { multiplier = p->cellPtr()->getMaterial()->getReactionXS(p, rxn_type); }
  tally_hist += ( multiplier * p->wgt() / p->cellPtr()->getMaterial()->macro_xs(p) );
}

// Tally the distance that the particle traveled in cell along with particle weight.
void track_length_estimator::tallyflux( Particle* p, double d ) {
  if ( rxn_type != "None" ) { multiplier = p->cellPtr()->getMaterial()->getReactionXS(p, rxn_type); }
  // std::cout << p->alive() << "\n";
  tally_hist += ( multiplier * d * p->wgt() );
}

void collision_estimator::tallyflux( Particle* p, double d ) {}

void counting_estimator::endHistory() {
  if ( tally.size() < count_hist + 1 ) { tally.resize( count_hist + 1, 0.0 ); }
  tally[ count_hist ] += 1.0;
  nhist++;
  count_hist = 0;
}

// void counting_estimator::endGeneration( double W ) {}
// void counting_estimator::reportFinal( int n_tracks, int rank, int world_size ) {}

// void surface_current_estimator::endGeneration( double W ) {}
// void surface_current_estimator::reportFinal( int n_tracks, int rank, int world_size ) {}

void counting_estimator::report( int n_tracks, int rank, int world_size ) {
  if (rank == 0) { std::cout << name() << std::endl; };
  double s1 = 0.0, s2 = 0.0;
  for ( int i = 0 ; i < tally.size() ; i++ ) {
    double p = tally[i] / nhist;
    #ifdef _MPI
      double global_p;
      MPI_Reduce(&p,&global_p,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
      global_p /= world_size;
      p = global_p;
    #endif
    double sdom = std::sqrt( p * ( 1.0 - p ) / ( nhist * world_size ) ) / p;
    if (rank == 0) { std::cout << std::setw(2) << i << std::scientific << " " << p << "   " << sdom <<  std::endl; };
    s1 += p * i;
    s2 += p * i * i;
  }
  if (rank == 0) {
    std::cout << std::fixed << "   mean = " << s1 << std::endl;
    std::cout << "   var  = " << s2 - s1*s1 << std::endl;
  }
}

// Binning track length estimator
void counting_track_length_estimator::tallyflux( Particle* p, double d ) {
  if ( type == "Time" ) {
    double time_range = p->time_traveled( d ); // Get travel time for track
    double final_time = p->getTime() + time_range; // Get time at end of track
    for ( unsigned long long i = 0; i <= NumberOfIntervals; i++ ) { // Loop through all values in range
      double lower_bound = std::fmax( data[i].first, p->getTime() );
      double upper_bound = std::fmin( data[i+1].first, final_time );
      if ( upper_bound > lower_bound ) { // Get segment of track in current time bin
        if ( rxn_type != "None" ) { multiplier = p->cellPtr()->getMaterial()->getReactionXS(p, rxn_type); }
        data[i+1].second += ( multiplier * p->dist_traveled( upper_bound - lower_bound ) * p->wgt() );
      }
    }
  } else if ( type == "Energy" ) {
    for ( unsigned long long i = 0; i <= NumberOfIntervals; i++ ) { // Loop through all values in range
      double lower_bound = data[i].first;
      double upper_bound = data[i+1].first;
      if ( p->energy() > lower_bound && p->energy() < upper_bound ) {
        if ( rxn_type != "None" ) { multiplier = p->cellPtr()->getMaterial()->getReactionXS(p, rxn_type); }
        data[i+1].second += ( multiplier * d * p->wgt() );
      }
    }
  }
}

void counting_track_length_estimator::report( int n_tracks, int rank, int world_size ) {
  if (rank == 0) { std::cout << name() << std::endl; };
  for ( int i = 1 ; i <= NumberOfIntervals + 1; i++ ) {
    double p = data[i].second / nhist;
    #ifdef _MPI // Average values from all processors
      double global_p;
      MPI_Reduce(&p,&global_p,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
      global_p /= world_size;
      p = global_p;
    #endif
    double sdom = std::sqrt( p * ( 1.0 - p ) / ( nhist * world_size ) ) / p;
    if (rank == 0) { std::cout << std::scientific << data[i].first << " " << p << "   " << sdom <<  std::endl; };
  }
}

// Binning collision estimator
void counting_collision_estimator::tallyflux( Particle* p, double d ) {
  if ( type == "Time" ) {
    double time_range = p->time_traveled( d ); // Get travel time for track
    double final_time = p->getTime() + time_range; // Get time at end of track
    for ( unsigned long long i = 0; i <= NumberOfIntervals; i++ ) { // Loop through all values in range
      double lower_bound = std::fmax( data[i].first, p->getTime() );
      double upper_bound = std::fmin( data[i+1].first, final_time );
      if ( upper_bound > lower_bound ) { // Get segment of track in current time bin
        if ( rxn_type != "None" ) { multiplier = p->cellPtr()->getMaterial()->getReactionXS(p, rxn_type); }
        data[i+1].second += ( multiplier * p->wgt() / p->cellPtr()->getMaterial()->macro_xs(p) );
      }
    }
  } else if ( type == "Energy" ) {
    for ( unsigned long long i = 0; i <= NumberOfIntervals; i++ ) { // Loop through all values in range
      double lower_bound = data[i].first;
      double upper_bound = data[i+1].first;
      if ( p->energy() > lower_bound && p->energy() < upper_bound ) {
        if ( rxn_type != "None" ) { multiplier = p->cellPtr()->getMaterial()->getReactionXS(p, rxn_type); }
        data[i+1].second += ( multiplier * p->wgt() / p->cellPtr()->getMaterial()->macro_xs(p) );
      }
    }
  }
}

void counting_collision_estimator::report( int n_tracks, int rank, int world_size ) {
  if (rank == 0) { std::cout << name() << std::endl; };
  for ( int i = 1 ; i <= NumberOfIntervals + 1; i++ ) {
    double p = data[i].second / nhist;
    #ifdef _MPI // Average values from all processors
      double global_p;
      MPI_Reduce(&p,&global_p,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
      global_p /= world_size;
      p = global_p;
    #endif
    double sdom = std::sqrt( p * ( 1.0 - p ) / ( nhist * world_size ) ) / p;
    if (rank == 0) { std::cout << std::scientific << data[i].first << " " << p << "   " << sdom <<  std::endl; };
  }
}

void pulse_height_estimator::report( int n_tracks, int rank, int world_size ) {
  if (rank == 0) { std::cout << name() << std::endl; };
  for ( int i = 1 ; i <= NumberOfIntervals + 1; i++ ) {
    double p = data[i].second / nhist;
    #ifdef _MPI // Average values from all processors
      double global_p;
      MPI_Reduce(&p,&global_p,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
      global_p /= world_size;
      p = global_p;
    #endif
    double sdom = std::sqrt( p * ( 1.0 - p ) / ( nhist * world_size ) ) / p;
    if (rank == 0) { std::cout << std::scientific << data[i].first << " " << p << "   " << sdom <<  std::endl; };
  }
}

