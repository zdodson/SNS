#include <vector>
#include <memory>
#include <cassert>

#include "Random.h"
#include "Nuclide.h"

// add a new reaction to the current nuclide
void Nuclide::addReaction( std::shared_ptr< reaction > R ) {
  rxn.push_back( R );
}

// return the total microscopic cross section
double Nuclide::total_xs(Particle* p) {
  double xs = 0.0;
  for ( auto r : rxn ) { xs += r->xs(p); }
  return xs;
}

double Nuclide::fiss_xs(Particle* p) {
  double fiss_xs = 0.0;
  for ( auto r : rxn ) {
    if ( r->name() == "Fission" ) {
      fiss_xs = r->xs(p);
    }
  }
  return fiss_xs;
}

double Nuclide::cap_xs(Particle* p) {
  double cap_xs = 0.0;
  for ( auto r : rxn ) {
    if ( r->name() == "Capture" ) {
      cap_xs = r->xs(p);
    }
  }
  return cap_xs;
}

double Nuclide::scat_xs(Particle* p) {
  double scat_xs = 0.0;
  for ( auto r : rxn ) {
    if ( r->name() == "Scatter" ) {
      scat_xs = r->xs(p);
    }
  }
  return scat_xs;
}

// randomly sample a reaction type from this nuclide
std::shared_ptr< reaction > Nuclide::sample_reaction(Particle* p) {
  double u = total_xs(p) * Urand();
  double s = 0.0;
  for ( auto r : rxn ) {
    s += r->xs(p);
    if ( s > u ) { return r; }
  }
  assert( false ); // should never reach here
  return nullptr;
}

std::shared_ptr< reaction > Nuclide::get_scatter_reaction()
{
  for (auto r : rxn)
  {
    if (r->name() == "Scatter")
    {
      return(r);
    }
  }
  assert(false);
  return nullptr;
}

void Nuclide::setMatTempPtr( double* T ) {
  MatTempPtr = T;
  for ( auto r : rxn ) {
    r->setMatTempPtr( T );
  }
}
