#include "Source.h"

// Sample from the source position and direction distributions and return particle
Particle Source::sample( std::vector< std::shared_ptr< Cell > > c_list ) {
  Particle p = Particle( dist_pos->sample(), dist_dir->sample(), dist_e->sample(), dist_time->sample() );
  for ( auto c : c_list ) { // Determine the cell that the particle is born in
    if ( c->testPoint( p.pos() ) ) {
      p.recordCell( c );
      if ( p.cellPtr()->getImportance() == 0.0 ) {
        p.kill();
      }
    }
  }
	for(auto e : p.cellPtr()->getEstimators()){
		e->add_entrant_energy(&p);
	}
  return p;
}
