#ifndef _PROCESSINPUT_HEADER_
#define _PROCESSINPUT_HEADER_

#include <string>
#include <memory>
#include <vector>
#include <iostream>
#include <cassert>
#include <limits>
#include <cmath>

#include "pugixml.hpp"
#include "Distribution.h"
#include "Reaction.h"
#include "Nuclide.h"
#include "Material.h"
#include "Surface.h"
#include "Cell.h"
#include "Source.h"
#include "Geometry.h"

std::shared_ptr<Geometry> ProcessInput( std::string );

#endif