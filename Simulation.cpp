#include "Simulation.h"



void FixedSourceSimulation( std::shared_ptr<Geometry> Geom, double rank, double world_size ) {
  std::pair<unsigned long long, unsigned long long> loop_range;
  double rankparticles = std::round(Geom->get_Nparticles() / world_size);
  loop_range = std::make_pair( rank*rankparticles + 1, (rank+1)*rankparticles );
  Geom->ActivateHistories();
  
  if ( rank == 0 ) { Geom->addHistories( Geom->get_Nparticles() * world_size ); Geom->start_clock(); }
  for ( unsigned long long i = loop_range.first ; i <= loop_range.second ; i++ ) {
    RN_init_particle( &i );
    std::stack< Particle > bank; // Initialize a particle stack.
    bank.push( Geom->getSourceParticle() ); // Add a particle to the bank.
    // History loop
    while ( ! bank.empty() ) {
      Particle p = bank.top(); bank.pop(); // Copy p to working particle and pop particle off bank (reduces size by 1).
      // Particle loop
      while ( p.alive() ) {
        // if ( isnan(p.pos().x) ) { p.kill(); };
        // std::cout << i << "\n";
        double distColl = p.cellPtr()->sampleDistColl(&p); // Sample distance to next collision
        std::pair< std::shared_ptr< Surface >, double > S = p.cellPtr()->SurfaceIntersect( p.getRay() ); // Get vector containing closest surface and distance to surface
        double dist_surface = S.second; // Distance to surface
        double distance = std::fmin( distColl, dist_surface ); // get the true distance the particle travels
        p.cellPtr()->moveParticle( &p, distance, Geom->getActiveFlag() ); // move the particle
        
        if ( distance == dist_surface ) { // If particle makes it to surface w/o colliding...
          S.first->crossSurface( &p , distance , p.cellPtr()->getExponentialTransform() , 
                                  p.cellPtr()->macro_xs( &p ) , p.cellPtr()->getTransformParameter() ); // Particle crosses surface
          Geom->changeCell( &p, &bank ); // Particle changes cells. Variance reduction is also done in this routine.
        } else { // else the particle has collided
          for ( auto m : Geom->MeshList ) { m->score( &p ); }
          p.cellPtr()->sampleCollision( &p, &bank, Geom->get_TimeLimit(), Geom->getActiveFlag() , distance ); // Sample and handle collision
        } // end event if-then
        Geom->increment_N_tracks(); // Increment the number of tracks in simulation
      } // end particle loop
    } // end history loop
    for ( auto e : Geom->EstList ) { e->endHistory(); } // End history for all estimators
    for ( auto m : Geom->MeshList ) { m->endHistory(); } // End history for all mesh estimators
    // int progress = 100 * ( (double)( i - loop_range.first ) / (double)( loop_range.second - loop_range.first ) );
    // if ( rank == 0 && progress % 10 == 0 ) {
    //   std::cout << std::fixed << std::setprecision(1) << "Progress: " << progress << "%\r";
    //   std::cout.flush();
    // }
  } // end simulation loop
  if (rank == 0) { std::cout << std::scientific << std::setprecision(5) << std::endl; } // skip line
  if ( rank == 0 ) { Geom->finalize(); }
  for ( auto e : Geom->EstList ) { e->report( Geom->get_N_tracks(), rank, world_size ); } // Report all results
  for ( auto m : Geom->MeshList ) { m->report( Geom->get_N_tracks(), rank, world_size ); } // Report all results
  #ifdef _MPI
    MPI_Finalize();
  #endif
}

void EigenvalueSimulation( std::shared_ptr<Geometry> Geom, double rank, double world_size ) {
  std::stack<Particle> source_bank;
  std::stack<Particle> fission_bank;
  
  for ( unsigned long long j = 0 ; j <= Geom->get_Nparticles() ; j++ ) { source_bank.push( Geom->getSourceParticle() ); }
  
  std::pair<unsigned long long, unsigned long long> loop_range;
  loop_range = std::make_pair( 1, Geom->getTotalCycles() );
  
  Geom->init_k_problem();
  for ( unsigned long long i = loop_range.first ; i <= loop_range.second ; i++ ) {
    Geom->reset_k_gen();
    
    double wgt = ( Geom->get_Nparticles() ) / ( (double)source_bank.size() );
    // unsigned long long particle_number = i / Geom->get_Nparticles();
    // RN_init_particle( &particle_number );

    while ( ! source_bank.empty() ) {
      std::stack< Particle > bank; // Initialize a particle stack.
      bank.push( source_bank.top() ); source_bank.pop();
      
      // History loop
      while ( ! bank.empty() ) {
        Particle p = bank.top(); bank.pop(); // Copy p to working particle and pop particle off bank (reduces size by 1).
        p.adjustWeight( wgt );
        
        // Particle loop
        while ( p.alive() ) {
          
          double distColl = p.cellPtr()->sampleDistColl(&p); // Sample distance to next collision
          std::pair< std::shared_ptr< Surface >, double > S = p.cellPtr()->SurfaceIntersect( p.getRay() ); // Get vector containing closest surface and distance to surface
          double dist_surface = S.second; // Distance to surface
          double distance = std::fmin( distColl, dist_surface ); // get the true distance the particle travels
          p.cellPtr()->moveParticle( &p, distance, Geom->getActiveFlag() ); // move the particle
          
          Geom->increment_k_tl( wgt * p.cellPtr()->getMaterial()->getNuSigf(&p) * distance );
          
          if ( distance == dist_surface ) { // If particle makes it to surface w/o colliding...
            
            S.first->crossSurface( &p , distance , p.cellPtr()->getExponentialTransform() , 
                                    p.cellPtr()->macro_xs( &p ) , p.cellPtr()->getTransformParameter() ); // Particle crosses surface
            Geom->changeCell( &p, &bank ); // Particle changes cells. Variance reduction is also done in this routine.
          } else { // else the particle has collided
            
            if ( Geom->getActiveFlag() ) { for ( auto m : Geom->MeshList ) { m->score(&p); }; }
            
            Geom->increment_k_col( wgt * p.cellPtr()->getMaterial()->getf(&p) );
            // bank any fission neutrons
            
            Geom->add_to_fission_bank( wgt, &p, &fission_bank );
            
            p.cellPtr()->sampleCollision( &p, &bank, Geom->get_TimeLimit(), Geom->getActiveFlag() , distance ); // Sample and handle collision
          } // end event if-then
          Geom->increment_N_tracks(); // Increment the number of tracks in simulation
        } // end particle loop
      } // end history loop
      if ( Geom->getActiveFlag() ) { for ( auto e : Geom->EstList ) { e->endHistory(); }; } // End history for all estimators
      if ( Geom->getActiveFlag() ) { for ( auto m : Geom->MeshList ) { m->endHistory(); }; } // End history for all estimators
    }
    
    source_bank.swap( fission_bank );
    Geom->end_k_gen( i, source_bank.size() );
    
  } // end simulation loop
  if ( rank == 0 ) { std::cout << std::endl; } // skip line
  if ( rank == 0 ) { Geom->finalize(); }
  for ( auto e : Geom->EstList ) { e->setNHistories( Geom->get_Nparticles() * Geom->getActiveCycles() ); } // set number of histories
  for ( auto e : Geom->EstList ) { e->report( Geom->get_N_tracks(), rank, world_size ); } // Report all results
  // Geom->finalizeEstimators( rank );
  for ( auto m : Geom->MeshList ) { m->setNHistories( Geom->get_Nparticles() * Geom->getActiveCycles() ); } // set number of histories
  for ( auto m : Geom->MeshList ) { m->report( Geom->get_N_tracks(), rank, world_size ); } // Report all results
  #ifdef _MPI
    MPI_Finalize();
  #endif
  
}
