#define CATCH_CONFIG_MAIN

#include <vector>

#include "Catch.h"
#include "Mesh.h"


TEST_CASE( "CartesianMesh", "[cartesianMesh]" ) {
  
    std::vector< double > origin = { -5.0, -4.0, 3.0 };
    std::vector< double > extent = { 1.0, 2.0, 3.0 };
    std::vector< int > intervals = { 4, 3, 2 };
    
    std::shared_ptr< Tally > testTally = std::make_shared< Tally >( "testTally", "None", 1.0 );
    std::vector< std::shared_ptr< Tally > > TL;
    TL.push_back( testTally );
    
    CartesianMesh CartMesh = CartesianMesh( "meshFile", origin, extent, intervals, TL );
    
    std::shared_ptr<reaction> R = std::make_shared<capture_reaction>( capture_reaction( 1.0, 0.0, "blank", 1, nullptr, true ) );
    std::shared_ptr<Nuclide> N = std::make_shared<Nuclide>( "testNuclide" );
    N->addReaction( R );
    std::shared_ptr<Material> M = std::make_shared<Material>( "teatMaterial", 1.0 );
    M->addNuclide( N, 1.0 );
    std::shared_ptr<Cell> C = std::make_shared<Cell>( "testCell" );
    C->setMaterial( M );
    
    Particle p_inmesh = Particle( point( -4.51, -2.5, 3.2 ), point( 0.0, 0.0, 1.0 ), 1.0, 0.0 );
    Particle p_outmesh = Particle( point( -5.5, -2.5, 3.2 ), point( 0.0, 0.0, 1.0 ), 1.0, 0.0 );
    
    p_inmesh.recordCell( C );
    // test that particle p_inmesh is in the mesh
    SECTION ( " test if particle is in mesh " ) {
      REQUIRE( CartMesh.in_mesh( &p_inmesh ) );
    }
    
    // test that particle p_outmesh is not in the mesh
    SECTION ( " test if particle is not in mesh " ) {
      REQUIRE( ! CartMesh.in_mesh( &p_outmesh ) );
    }
    
    // test that particle p_inmesh is in the correct bin
    SECTION ( " test if particle is in correct bin " ) {
      REQUIRE( CartMesh.lookup_bin( &p_inmesh ) == 9 );
    }
    
    std::pair< std::vector< std::vector< double > >, std::vector<double> > points = CartMesh.get_points( 9 );
    
    SECTION ( " test bounding points for bin " ) {
      REQUIRE( points.first[0][0] == -4.75            );
      REQUIRE( points.first[0][1] == Approx(-2.66667) );
      REQUIRE( points.first[0][2] == 3                );
      REQUIRE( points.first[1][0] == -4.75            );
      REQUIRE( points.first[1][1] == Approx(-2.66667) );
      REQUIRE( points.first[1][2] == 4.5              );
      REQUIRE( points.first[2][0] == -4.75            );
      REQUIRE( points.first[2][1] == -2               );
      REQUIRE( points.first[2][2] == 3                );
      REQUIRE( points.first[3][0] == -4.75            );
      REQUIRE( points.first[3][1] == -2               );
      REQUIRE( points.first[3][2] == 4.5              );
      REQUIRE( points.first[4][0] == -4.5             );
      REQUIRE( points.first[4][1] == Approx(-2.66667) );
      REQUIRE( points.first[4][2] == 3                );
      REQUIRE( points.first[5][0] == -4.5             );
      REQUIRE( points.first[5][1] == Approx(-2.66667) );
      REQUIRE( points.first[5][2] == 4.5              );
      REQUIRE( points.first[6][0] == -4.5             );
      REQUIRE( points.first[6][1] == -2               );
      REQUIRE( points.first[6][2] == 3                );
      REQUIRE( points.first[7][0] == -4.5             );
      REQUIRE( points.first[7][1] == -2               );
      REQUIRE( points.first[7][2] == 4.5              );
    }
    
    // test bin central point
    SECTION( " test central point for bin " ) {
      REQUIRE( points.second[0] == -4.625             );
      REQUIRE( points.second[1] == Approx(-2.33333)   );
      REQUIRE( points.second[2] == 3.75               );
    }
    
    // check element volume
    SECTION( " test element volume " ) {
      REQUIRE( CartMesh.getElementVolume( 9 ) == Approx( 0.25 ) );
    }
    
    // test scoring routine
    SECTION( " test scoring routine " ) {
      CartMesh.score( &p_inmesh );
      REQUIRE( CartMesh.getTallyList()[0]->getHistValue( 9 ) == Approx( 4.0 ) );
    }

}

TEST_CASE( "CylindricalMesh", "[cylindricalMesh]" ) {
  
    std::vector< double > origin = { 0.0, 0.0, 0.0 };
    std::vector< double > extent = { 1.0, 2.0, 0.0 };
    std::vector< int > intervals = { 2, 2, 8 };
    
    std::shared_ptr< Tally > testTally = std::make_shared< Tally >( "testTally", "None", 1.0 );
    std::vector< std::shared_ptr< Tally > > TL;
    TL.push_back( testTally );
    
    CylindricalMesh CylMesh = CylindricalMesh( "meshFile", origin, extent, intervals, TL );
    
    std::shared_ptr<reaction> R = std::make_shared<capture_reaction>( capture_reaction( 1.0, 0.0, "blank", 1, nullptr, true ) );
    std::shared_ptr<Nuclide> N = std::make_shared<Nuclide>( "testNuclide" );
    N->addReaction( R );
    std::shared_ptr<Material> M = std::make_shared<Material>( "teatMaterial", 1.0 );
    M->addNuclide( N, 1.0 );
    std::shared_ptr<Cell> C = std::make_shared<Cell>( "testCell" );
    C->setMaterial( M );
    
    Particle p_inmesh = Particle( point( 0.49, 0.1, 1.2 ), point( 0.0, 0.0, 1.0 ), 1.0, 0.0 );
    Particle p_outmesh = Particle( point( -5.0, -2.5, 3.2 ), point( 0.0, 0.0, 1.0 ), 1.0, 0.0 );
    
    p_inmesh.recordCell( C );
    // test that particle p_inmesh is in the mesh
    SECTION ( " test if particle is in mesh " ) {
      REQUIRE( CylMesh.in_mesh( &p_inmesh ) );
    }
    
    // test that particle p_outmesh is not in the mesh
    SECTION ( " test if particle is not in mesh " ) {
      REQUIRE( ! CylMesh.in_mesh( &p_outmesh ) );
    }
    
    // test that particle p_inmesh is in the correct bin
    SECTION ( " test if particle is in correct bin " ) {
      REQUIRE( CylMesh.lookup_bin( &p_inmesh ) == 24 );
    }
    
    // for ( int i ; i <= 30 ; i++ ) {
    //   std::cout << CylMesh.getElementVolume( i ) << "\n";
    // }
    
    // test bin volume
    // SECTION ( " test bin volume " ) {
    //   REQUIRE( CylMesh.getElementVolume( 24 ) == Approx( 0.14726 ) );
    // }
    
    // std::pair< std::vector< std::vector< double > >, std::vector<double> > points = CylMesh.get_points( 9 );
    
    // SECTION ( " test bounding points for bin " ) {
    //   REQUIRE( points.first[0][0] == -4.75            );
    //   REQUIRE( points.first[0][1] == Approx(-2.66667) );
    //   REQUIRE( points.first[0][2] == 3                );
    //   REQUIRE( points.first[1][0] == -4.75            );
    //   REQUIRE( points.first[1][1] == Approx(-2.66667) );
    //   REQUIRE( points.first[1][2] == 4.5              );
    //   REQUIRE( points.first[2][0] == -4.75            );
    //   REQUIRE( points.first[2][1] == -2               );
    //   REQUIRE( points.first[2][2] == 3                );
    //   REQUIRE( points.first[3][0] == -4.75            );
    //   REQUIRE( points.first[3][1] == -2               );
    //   REQUIRE( points.first[3][2] == 4.5              );
    //   REQUIRE( points.first[4][0] == -4.5             );
    //   REQUIRE( points.first[4][1] == Approx(-2.66667) );
    //   REQUIRE( points.first[4][2] == 3                );
    //   REQUIRE( points.first[5][0] == -4.5             );
    //   REQUIRE( points.first[5][1] == Approx(-2.66667) );
    //   REQUIRE( points.first[5][2] == 4.5              );
    //   REQUIRE( points.first[6][0] == -4.5             );
    //   REQUIRE( points.first[6][1] == -2               );
    //   REQUIRE( points.first[6][2] == 3                );
    //   REQUIRE( points.first[7][0] == -4.5             );
    //   REQUIRE( points.first[7][1] == -2               );
    //   REQUIRE( points.first[7][2] == 4.5              );
    // }
    
    // // test bin central point
    // SECTION( " test central point for bin " ) {
    //   REQUIRE( points.second[0] == -4.625             );
    //   REQUIRE( points.second[1] == Approx(-2.33333)   );
    //   REQUIRE( points.second[2] == 3.75               );
    // }
    
    // // check element volume
    // SECTION( " test element volume " ) {
    //   REQUIRE( CylMesh.getElementVolume( 9 ) == Approx( 0.25 ) );
    // }
    
    // // test scoring routine
    // SECTION( " test scoring routine " ) {
    //   CylMesh.score( &p_inmesh );
    //   REQUIRE( CylMesh.getTallyList()[0]->getHistValue( 9 ) == Approx( 4.0 ) );
    // }

}