#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <limits>
#include <string>
#include <vector>
#include <iostream>

#include "Catch.h"
#include "Surface.h"
#include "Random.h"
#include "Estimator.h"

TEST_CASE( "PulseHeight", "[pulseheight]" ) {
	
	
	// test returns appropriate vector for pulse height estimator
    SECTION ( " return expected data vector given an energy deposit of 3.5 MeV " ) {
		point pt1;
		double p_energy_in 			= 8.0;
		Particle p1(pt1, pt1, p_energy_in, 0.0);
		std::string e1_label 		= "estimator 1";
		double min_bound 			= 0.0;
		double max_bound		 	= 10.0;
		double NumberOfIntervals 	= 10.0;
		bool logflag 				= false;
		double delta 				= ( max_bound - min_bound ) / NumberOfIntervals;
		pulse_height_estimator e1(e1_label, min_bound, max_bound, NumberOfIntervals, logflag);
		//add the entrant energy to e1
		e1.add_entrant_energy(&p1);
		
		// change particle energy and add an exit energy
		double resulting_energy 	= 4.5;
		p1.set_energy(resulting_energy);
		e1.add_exit_energy(&p1);
		
		// end this history; nh = 1;
		e1.endHistory();
		
		// set up expected vector
		
		std::vector< std::pair< double, double > > expected_data;
		expected_data.push_back( std::make_pair( 0.0, 0.0 ) );
		for (double i = min_bound ; i <= max_bound; i += delta){
			expected_data.push_back( std::make_pair( i, 0.0 ) );
		}
		expected_data.at(4).second += 1.0;
		for (double i = min_bound ; i <= max_bound; i += delta){
			REQUIRE(e1.get_data().at(i).first == Approx(expected_data.at(i).first));
			REQUIRE(e1.get_data().at(i).second == Approx(expected_data.at(i).second));
		}
	}
	
	// test returns appropriate vector for pulse height estimator
    SECTION ( " return expected data vector given several energy deposits on a log scale" ) {
	

		// now a log scale test with multiple values stored
		double min_bound 				= 0.01;
		double max_bound		 		= 100.0;
		double NumberOfIntervals 		= 4.0;
		bool logflag 					= true;
		std::string e2_label 	= "pulse height 2";
		pulse_height_estimator e2(e2_label, min_bound, max_bound, NumberOfIntervals, logflag);
		
		//set up a logspace vector for energy to compare results
		double logMin = std::log10(min_bound);
		double logMax = std::log10(max_bound);
		double del = (logMax - logMin) / NumberOfIntervals;
		double accDelta = 0.0;
		std::vector<double> E_log_vec;
		for (int i = 0; i <= NumberOfIntervals; i++ ) {
			  E_log_vec.push_back( std::pow( 10, logMin + accDelta) );
			  accDelta += del;// accDelta = delta * i
		}
		
		std::vector< std::pair< double, double > > expected_data;
		expected_data.push_back( std::make_pair( 0.0, 0.0 ) );
		for ( int i = 0; i <= NumberOfIntervals; i++ ){
			expected_data.push_back( std::make_pair( E_log_vec.at(i), 0.0 ) );
		}
		
		// perform several energy deposits in e2
	
		point pt1;
		double p_energy_in 			= 100.0;
		Particle p1(pt1, pt1, p_energy_in, 0.0);
		
		// history 1: re-entrant energy deposit of 65 MeV
		e2.add_entrant_energy(&p1);
		p1.set_energy(40.0);
		e2.add_exit_energy(&p1);
		p1.set_energy(45.0);
		e2.add_entrant_energy(&p1);
		p1.set_energy(40.0);
		e2.add_exit_energy(&p1);
		e2.endHistory();
		
		// history 2: small singular energy deposit of 0.02 MeV
		p1.set_energy(0.03);
		e2.add_entrant_energy(&p1);
		p1.set_energy(0.01);
		e2.add_exit_energy(&p1);
		e2.endHistory();
		
		// history 3: 3 re-entrances for an accumulated energy deposit of 7 MeV
		p1.set_energy(8.0);
		e2.add_entrant_energy(&p1);
		p1.set_energy(7.0);
		e2.add_exit_energy(&p1);
		p1.set_energy(6.5);
		e2.add_entrant_energy(&p1);
		p1.set_energy(4.5);
		e2.add_exit_energy(&p1);
		e2.add_entrant_energy(&p1);
		p1.set_energy(0.5);
		e2.add_exit_energy(&p1);
		e2.endHistory();
		
		// history 4: energy deposit of 5 MeV
		p1.set_energy(10.0);
		e2.add_entrant_energy(&p1);
		p1.set_energy(5.0);
		e2.add_exit_energy(&p1);
		e2.endHistory();
		
		// the final expected data vector should be :
		// <<0.01,   1.0>
		//  < 0.1,   0.0>
		//  < 1.0,   2.0>
		//  < 10.0,  1.0> 
		//  < 100.0, 0.0>
		
		expected_data.at(1).second += 1.0;
		expected_data.at(3).second += 2.0;
		expected_data.at(4).second += 1.0;
		
		for (int i = 0; i <= NumberOfIntervals; i++ ){
			REQUIRE(e2.get_data().at(i).first == Approx(expected_data.at(i).first));

			REQUIRE(e2.get_data().at(i).second == Approx(expected_data.at(i).second));
		}

	}
}