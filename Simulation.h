#ifndef _SIMULATION_HEADER_
#define _SIMULATION_HEADER_

#include <memory>
#include <iostream>
#include <stack>
#include <iomanip>

#include "Distribution.h"
#include "Nuclide.h"
#include "Material.h"
#include "Particle.h"
#include "Geometry.h"
#include "Random.h"
#include "Source.h"
#include "Cell.h"
#include "Estimator.h"
#include "ProcessInput.h"
#include "Mesh.h"
#ifdef _MPI
  #include "mpi.h"
#endif

void FixedSourceSimulation( std::shared_ptr<Geometry>, double rank, double world_size );
void EigenvalueSimulation( std::shared_ptr<Geometry>, double rank, double world_size );

#endif