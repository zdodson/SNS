#ifndef _GEOMETRY_HEADER_
#define _GEOMETRY_HEADER_

#include "Particle.h"
#include "Cell.h"
#include "Source.h"
#include "Random.h"
#include "Mesh.h"
#include <vector>
#include <memory>
#include <limits>
#include <ctime>

// Geometry is just a list of cells.
class Geometry {
  private:
    int N_cell           = 0;     // Number of cells.
    int N_src            = 0;     // Number of sources.
    int N_est            = 0;     // Number of estimators.
    int N_particles      = 10000; // Number of particles to run (changed by input file)
    double timelimit     = std::numeric_limits<double>::max();
    double keff_guess    = 1.0;
    int inactiveCycles   = 0;
    int activeCycles     = 1;
    int number_of_tracks = 0;     // Number of tracks in entire geometry
    std::vector<double> src_cdf;  // CDF for sampling the source that will emit a particle.
    std::string simulation_name;  // The name of the case to be run.
    std::string run_type;  // The type of problem to be run.
    bool activeCycleFlag = false;
    std::clock_t Clock;
    unsigned long long nhist = 0;
    
    std::vector< double > k_col = { 0.0, 0.0, 0.0 }; // k collision    value, average, and variance
    std::vector< double > k_tl  = { 0.0, 0.0, 0.0 }; // k track length value, average, and variance
    
    std::pair< double, double > k_next = std::make_pair( 0.0, 0.0 ); // next value for k_col and k_tl
    std::pair< double, double > k_running = std::make_pair( 0.0, 0.0 ); // next value for k_col and k_tl
    std::pair< double, double > k_err = std::make_pair( 0.0, 0.0 ); // next value for k_col and k_tl
    
    std::vector< double > k_av = { 0.0, 0.0, 0.0 }; // global k, running average global k, and global error in k
    
    double entropy     = 0.0;
    double entropy_sum = 0.0;
    
    unsigned nactive;
    int neutrons;
    
    std::shared_ptr< Distribution<point> > isotropic = std::make_shared< isotropicDirection_Distribution > ( "isotropic" );
    std::shared_ptr<Distribution <double> > scatterDist = std::make_shared<uniform_Distribution> ("Isotropic Scatter", -1.0, 1.0);
    
    std::shared_ptr< ShannonMesh > shannon_mesh;
  public:
    std::vector< std::shared_ptr< Cell > > CellList;     // List of regions.
    std::vector< std::shared_ptr< Source > > SrcList;    // List of sources.
    std::vector< std::shared_ptr< Estimator > > EstList; // List of estimators.
    std::vector< std::shared_ptr< Mesh > > MeshList; // List of estimators.
    // std::vector< std::shared_ptr< flux_estimator > > singleValueCellEstimators; // List of estimators.
     Geometry() {};
    ~Geometry() {};

    void            changeCell( Particle* p, std::stack<Particle>* bank         );
    void         determineCell( Particle* p                                     );
    void              addCells( std::vector< std::shared_ptr<Cell> > CelList    ) { CellList = CelList;      };
    void            addSources( std::vector< std::shared_ptr<Source> > SList    );
    void         addEstimators( std::vector< std::shared_ptr<Estimator> > EList ) { EstList = EList; };
    void           addMeshList( std::vector< std::shared_ptr<Mesh> > MList      ) { MeshList = MList; };
    void        set_Nparticles( double N_par                                    ) { N_particles = N_par;     };
    void         set_TimeLimit( double TimeLimit                                ) { timelimit = TimeLimit;   };
    void               setType( std::string type                                );
    void           setInactive( double ninact                                   ) { inactiveCycles = ninact; };
    void             setActive( double nact                                     ) { activeCycles = nact;     };
    void         setkeff_guess( double k                                        ) { keff_guess = k;          };
    void              set_name( std::string name                                ) { simulation_name = name;  };
    void        setShannonMesh( std::shared_ptr<ShannonMesh> SM                 ) { shannon_mesh = SM;       };
    void      incrementEntropy( Particle*, int                                  );
    void     ActivateHistories()                                                  { activeCycleFlag = true;  };
    void    increment_N_tracks()                                                  { number_of_tracks++;      };
    int           get_N_tracks()                                                  { return number_of_tracks; };
    int         get_Nparticles()                                                  { return N_particles;      };
    double       get_TimeLimit()                                                  { return timelimit;        };
    std::string        getName()                                                  { return simulation_name;  };
    std::string        getType()                                                  { return run_type;         };
    double       getkeff_guess()                                                  { return keff_guess;       };
    int         getTotalCycles()                                                  { return inactiveCycles + activeCycles; };
    bool         getActiveFlag()                                                  { return activeCycleFlag;  };
    void             printInfo();
    Particle getSourceParticle();
    bool determineCellBON( Particle* p );
    void           start_clock()                                                  { Clock = std::clock();    };
    void              finalize();
    void          addHistories( int n                                           ) { nhist += n;              };
    void        init_k_problem();
    void       increment_k_col( double k )                                        { k_next.first +=  k;      };
    void        increment_k_tl( double k )                                        { k_next.second += k;      };
    void           reset_k_gen();
    void             end_k_gen( unsigned long long i, int s );
    void   add_to_fission_bank( double wgt, Particle* p, std::stack<Particle>* fission_bank );
    int        getActiveCycles(                                                 ) { return activeCycles;     };

};

#endif