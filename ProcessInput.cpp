#include "ProcessInput.h"

// function that returns an item from a vector of objects of type T by name provided
// the object class has a string and a method called name() allowing for it to be returned
template< typename T >
std::shared_ptr< T > findByName( std::vector< std::shared_ptr< T > > vec, std::string name ) {
  for ( auto v : vec ) {
    if ( v->name() == name ) { return v; }
  }
  return nullptr;
}

std::shared_ptr<Geometry> ProcessInput( std::string input_file_name ) {
  pugi::xml_document input_file;
  pugi::xml_parse_result load_result = input_file.load_file( input_file_name.c_str() );
  std::shared_ptr<Geometry> Geom = std::make_shared<Geometry>();
  const double twopi  = 2.0 * std::acos(-1.0);
  const double fourpi = 4.0 * std::acos(-1.0);

  // check to see if result failed and throw an exception if it did
  if ( ! load_result ) {
    std::cout << load_result.description() << std::endl;
    throw;
  }
  
  pugi::xml_node root_node = input_file.child("MonteCarloSimulation");
  Geom->set_name( root_node.attribute("Name").value() );
  Geom->set_Nparticles( root_node.attribute("Histories").as_int() );
  if ( root_node.attribute("TimeLimit") ) {
    Geom->set_TimeLimit( root_node.attribute("TimeLimit").as_double() );
  }
  if ( root_node.attribute("Type") ) {
    Geom->setType( root_node.attribute("Type").value() );
    if ( Geom->getType() == "Eigenvalue" ) {
      Geom->setInactive( root_node.attribute("InactiveCycles").as_int() );
      Geom->setActive( root_node.attribute("ActiveCycles").as_int() );
      Geom->setkeff_guess( root_node.attribute("keff").as_double() );
    }
  } else {
    std::cout << "Error, run type not specified!" << std::endl;
    throw;
  }
    
  // Distributions
  std::vector< std::shared_ptr< Distribution<double> > >  double_Distributions;
  std::vector< std::shared_ptr< Distribution<int>    > >  int_Distributions;
  std::vector< std::shared_ptr< Distribution<point>  > >  point_Distributions;
  
  pugi::xml_node input_Distributions = root_node.child("Distributions");
  // find total number of Distributions
  int num_Distributions = 0;
  for ( auto d : input_Distributions ) { num_Distributions++; }

  // since Distributions may depend on other Distributions, need to iterate
  int set_Distributions = 0;
  while ( set_Distributions < num_Distributions ) {
    int previous_set_Distributions = set_Distributions;

    for ( auto d : input_Distributions ) {
      std::string type = d.name();
      std::string name = d.attribute("Name").value();
      std::string data = d.attribute("Datatype").value();

      if ( data == "Double" ) {
        // skip rest of loop if Distribution already done
        if ( findByName( double_Distributions, name ) ) { continue; }

        std::shared_ptr< Distribution<double> > Dist;
        if ( type == "Delta" ) {
          double a = d.attribute("a").as_double();
          Dist = std::make_shared< arbitraryDelta_Distribution< double > > ( name, a );
        } else if ( type == "Uniform" ) {
          double a = d.attribute("a").as_double();
          double b = d.attribute("b").as_double();
          Dist = std::make_shared< uniform_Distribution > ( name, a, b );
        } else if ( type == "Linear" ) {
          double a  = d.attribute("a").as_double();
          double b  = d.attribute("b").as_double();
          double fa = d.attribute("fa").as_double();
          double fb = d.attribute("fb").as_double();
          Dist = std::make_shared< linear_Distribution > ( name, a, b, fa, fb );
        } else if ( type == "HenyeyGreenstein" ) {
          double a = d.attribute("a").as_double();
          Dist = std::make_shared< HenyeyGreenstein_Distribution > ( name, a );
        } else if ( type == "P6Dist" ) {
          double a = d.attribute("a").as_double();
          Dist = std::make_shared< Problem6_Distribution > ( name, a );
        } else if ( type == "WattFissionSpectrum" ) { 
          double a = d.attribute("a").as_double();
          double b = d.attribute("b").as_double();
          Dist = std::make_shared< WattFissionSpectrum > ( name, a, b );
        } else {
          std::cout << "unsupported Distribution with data type " << data << std::endl;
          throw;
        }
        double_Distributions.push_back( Dist );
      }
      // integer-valued Distributions
      else if ( data == "Int" ) {
        // skip rest of loop if Distribution already done
        if ( findByName( int_Distributions, name ) ) { continue; }

        std::shared_ptr< Distribution<int> > Dist;
        if ( type == "Delta" ) {
          double a = d.attribute("a").as_int();
          Dist = std::make_shared< arbitraryDelta_Distribution< int > > ( name, a );
        }
        else if ( type == "MeanMultiplicity" ) {
          double nubar = d.attribute("Nubar").as_double();
          Dist = std::make_shared< meanMultiplicity_Distribution > ( name );
          Dist->set_Nubar( nubar );
        }
        else if ( type == "TerrellFission" ) {
          double nubar = d.attribute("Nubar").as_double();
          double sigma = d.attribute("Sigma").as_double();
          double b     = d.attribute("b").as_double();
          Dist = std::make_shared< TerrellFission_Distribution > ( name, sigma, b, nubar );
        }
        else {
          std::cout << "unsupported Distribution with data type " << data << std::endl;
          throw;
        }
        int_Distributions.push_back( Dist );
      }
      else if ( data == "Point" ) {
        // skip rest of loop if Distribution already done
        if ( findByName( point_Distributions, name ) ) { continue; }

        std::shared_ptr< Distribution< point > > Dist;
        if ( type == "Delta" ) {
          double x = d.attribute("x").as_double(); 
          double y = d.attribute("y").as_double(); 
          double z = d.attribute("z").as_double();         
          Dist = std::make_shared< arbitraryDelta_Distribution< point > > ( name, point( x, y, z ) );
        } else if ( type == "Isotropic" ) {
          Dist = std::make_shared< isotropicDirection_Distribution > ( name );
        } else if ( type == "Anisotropic" ) {
          double u = d.attribute("u").as_double(); 
          double v = d.attribute("v").as_double(); 
          double w = d.attribute("w").as_double();         
          std::shared_ptr< Distribution<double> > angDist = 
            findByName( double_Distributions, d.attribute("Distribution").value() );
      
          // in the angular Distribution does not yet, skip to the end of the loop
          if ( ! angDist ) { continue; }

          Dist = std::make_shared< anisotropicDirection_Distribution > ( name, point( u, v, w ), angDist );
        } else if ( type == "IndependentXYZ" ) {
          std::shared_ptr< Distribution<double> > distX = findByName( double_Distributions, d.attribute("x").value() ); 
          std::shared_ptr< Distribution<double> > distY = findByName( double_Distributions, d.attribute("y").value() ); 
          std::shared_ptr< Distribution<double> > distZ = findByName( double_Distributions, d.attribute("z").value() ); 

          // if any of these Distributions have not yet been resolved, skip to the end of the loop
          if ( !distX || !distY || !distZ ) { continue; }

          Dist = std::make_shared< independentXYZ_Distribution > ( name, distX, distY, distZ );
        } else if ( type == "UniformSPH" ) {
          double r1 = 0.0, r2 = 1.0, mu1 = -1.0, mu2 = 1.0, phi1 = 0.0, phi2 = twopi;
          double x = d.attribute("x").as_double();
          double y = d.attribute("y").as_double();
          double z = d.attribute("z").as_double();
          if ( d.attribute("r1")   ) { r1   = d.attribute("r1").as_double();   }
          if ( d.attribute("r2")   ) { r2   = d.attribute("r2").as_double();   }
          if ( d.attribute("mu1")  ) { mu1  = d.attribute("mu1").as_double();  }
          if ( d.attribute("mu2")  ) { mu2  = d.attribute("mu2").as_double();  }
          if ( d.attribute("phi1") ) { phi1 = d.attribute("phi1").as_double(); }
          if ( d.attribute("phi2") ) { phi2 = d.attribute("phi2").as_double(); }
          std::shared_ptr< Distribution<double> > distR = std::make_shared< uniform_Distribution > ( "Rdist", r1 * r1 * r1, r2 * r2 * r2 );
          std::shared_ptr< Distribution<double> > distMU = std::make_shared< uniform_Distribution > ( "MUdist", mu1, mu2 );
          std::shared_ptr< Distribution<double> > distPHI = std::make_shared< uniform_Distribution > ( "PHIdist", phi1, phi2 );

          // if any of these Distributions have not yet been resolved, skip to the end of the loop
          if ( !distR || !distMU || !distPHI ) { continue; }

          Dist = std::make_shared< uniformSPH_Distribution > ( name, point( x, y, z ), distR, distMU, distPHI );
        } else if ( type == "UniformCYL" ) {
          double r1 = 0.0, r2 = 1.0, theta1 = 0.0, theta2 = twopi, z1 = 0.0, z2 = 1.0, thetaX = 0.0, thetaY = 0.0, thetaZ = 0.0;
          double x = d.attribute("x").as_double();
          double y = d.attribute("y").as_double();
          double z = d.attribute("z").as_double();
          if ( d.attribute("r1")     ) { r1     = d.attribute("r1").as_double();     }
          if ( d.attribute("r2")     ) { r2     = d.attribute("r2").as_double();     }
          if ( d.attribute("theta1") ) { theta1 = d.attribute("theta1").as_double(); }
          if ( d.attribute("theta2") ) { theta2 = d.attribute("theta2").as_double(); }
          if ( d.attribute("z1")     ) { z1     = d.attribute("z1").as_double();     }
          if ( d.attribute("z2")     ) { z2     = d.attribute("z2").as_double();     }
          if ( d.attribute("thetaX") ) { thetaX = d.attribute("thetaX").as_double(); }
          if ( d.attribute("thetaY") ) { thetaY = d.attribute("thetaY").as_double(); }
          if ( d.attribute("thetaZ") ) { thetaZ = d.attribute("thetaZ").as_double(); }
          std::shared_ptr< Distribution<double> > distR = std::make_shared< uniform_Distribution > ( "Rdist", r1 * r1, r2 * r2 );
          std::shared_ptr< Distribution<double> > distTHETA = std::make_shared< uniform_Distribution > ( "THETAdist", theta1, theta2 );
          std::shared_ptr< Distribution<double> > distZ = std::make_shared< uniform_Distribution > ( "Zdist", z1, z2 );

          // if any of these Distributions have not yet been resolved, skip to the end of the loop
          if ( !distR || !distTHETA || !distZ ) { continue; }

          Dist = std::make_shared< uniformCYL_Distribution > ( name, point( x, y, z ), distR, distTHETA, distZ, thetaX, thetaY, thetaZ );
        }
        else {
          std::cout << "unsupported " << data << " Distribution of type " << type << std::endl;
          throw;
        }
        point_Distributions.push_back( Dist );
      }
      else {
        std::cout << "unsupported Distribution with data type " << data << std::endl;
        throw;
      }
      // if we reach here, assume Distribution has been set
      set_Distributions++;
    }
    // check to see if number of Distributions has increased, if not, caught in an infinite loop
    if ( previous_set_Distributions == set_Distributions ) { 
      std::cout << "Distributions could not be resolved. " << std::endl;
      throw;
    }
  }
  
  // iterate over Nuclides
  std::vector< std::shared_ptr<Nuclide> > Nuclides;
  std::shared_ptr<FBotfdata> FBData = nullptr;
  pugi::xml_node input_Nuclides = root_node.child("Nuclides");
  for ( auto n : input_Nuclides ) {
    std::string name = n.attribute("Name").value();
    std::string fbotffile = "blank";
    bool no_otf = true;
    if ( n.attribute("OTFfile") ) {
      fbotffile = n.attribute("OTFfile").value();
      no_otf = false;
      FBData = std::make_shared<FBotfdata> ( fbotffile, no_otf );
    }
    std::shared_ptr< Nuclide > Nuc = std::make_shared< Nuclide > ( n.attribute("Name").value() );
    if ( n.attribute("A") ) {
      Nuc->setA( n.attribute("A").as_double() );
    }
    Nuclides.push_back( Nuc );

    Nuc->setOTFPtr(FBData);

    // iterate over its reactions
    for ( auto r : n.children() ) {
      std::shared_ptr< reaction > Rxn;
      std::string rxn_type = r.name();
      
      int ind = 0;
      bool disab = true;
      if ( r.attribute("otfix") ) {
        ind = r.attribute("otfix").as_int();
        disab = false;
      }

      std::string xs_filename = "blank";
      double a = 0.0;
      double b = 0.0;

      if ( r.attribute("xsfile") ) {
        xs_filename = r.attribute("xsfile").value();
      } else if ( r.attribute("a") ) {
        a = r.attribute("a").as_double();
        if ( r.attribute("b") ) {
          b = r.attribute("b").as_double();
        }
      }
      
      if ( rxn_type == "Capture" ) {
        Nuc->addReaction( std::make_shared< capture_reaction > ( a, b, xs_filename, ind, FBData, disab ) );
      } 
      else if ( rxn_type == "Scatter" ) {
        std::string dist_name = r.attribute("Distribution").value();
        std::shared_ptr< Distribution<double> > scatterDist = findByName( double_Distributions, dist_name );
        if ( scatterDist ) {
          Nuc->addReaction( std::make_shared< scatter_reaction > ( a, b, xs_filename, scatterDist, ind, FBData, disab ) );
        } else if ( r.attribute("Type") and ! r.attribute("Distribution") ) {
          std::string scat_type = r.attribute("Type").value();
          if ( scat_type == "Isotropic" ) {
            std::shared_ptr< Distribution<double> > IsoScat = std::make_shared< uniform_Distribution > ( "Isotropic Scatter", -1.0, 1.0 );
            Nuc->addReaction( std::make_shared< scatter_reaction > ( a, b, xs_filename, IsoScat, ind, FBData, disab ) );
          }
          else if ( scat_type == "Linear Anisotropic" && r.attribute("Mubar") ) {
            double mubar = r.attribute("Mubar").as_double();
            if ( -1.0 <= mubar <= 1.0 ) {
              double fa = ( 1 - 3 * mubar ) / fourpi;
              double fb = ( 1 + 3 * mubar ) / fourpi;
              std::shared_ptr< Distribution<double> > LinAnisoScat = std::make_shared< linear_Distribution > ( "Linearly Anisotropic Scattering", -1.0, 1.0, fa, fb );
              Nuc->addReaction( std::make_shared< scatter_reaction > ( a, b, xs_filename, LinAnisoScat, ind, FBData, disab ) );
            } else { std::cout << "Error, mubar = " << mubar << " is unphysical."; throw; }
          } else { std::cout << " unknown scattering type " << scat_type << " in Nuclide " << name << std::endl; }
        } else if ( r.attribute("Type") and r.attribute("Distribution") ) {
          std::cout << " Error, you have defined both the scattering type and a distribution. Please enter only one. " << std::endl;
        }
        else {
          std::cout << " unknown scattering Distribution " << dist_name << " in Nuclide " << name << std::endl;
          throw;
        }
      } else if ( rxn_type == "FreeGasScatter" ) {
        std::string dist_name = r.attribute("Distribution").value();
        std::shared_ptr< Distribution<double> > scatterDist = findByName( double_Distributions, dist_name );
        if ( scatterDist ) {
          Nuc->addReaction( std::make_shared< free_gas_scatter_reaction > ( a, b, xs_filename, scatterDist, Nuc->getAptr(), ind, FBData, disab ) );
        } else if ( r.attribute("Type") and ! r.attribute("Distribution") ) {
          std::string scat_type = r.attribute("Type").value();
          if ( scat_type == "Isotropic" ) {
            std::shared_ptr< Distribution<double> > IsoScat = std::make_shared< uniform_Distribution > ( "Isotropic Scatter", -1.0, 1.0 );
            Nuc->addReaction( std::make_shared< free_gas_scatter_reaction > ( a, b, xs_filename, IsoScat, Nuc->getAptr(), ind, FBData, disab ) );
          }
          else if ( scat_type == "Linear Anisotropic" && r.attribute("Mubar") ) {
            double mubar = r.attribute("Mubar").as_double();
            if ( -1.0 <= mubar <= 1.0 ) {
              double fa = ( 1 - 3 * mubar ) / fourpi;
              double fb = ( 1 + 3 * mubar ) / fourpi;
              std::shared_ptr< Distribution<double> > LinAnisoScat = std::make_shared< linear_Distribution > ( "Linearly Anisotropic Scattering", -1.0, 1.0, fa, fb );
              Nuc->addReaction( std::make_shared< free_gas_scatter_reaction > ( a, b, xs_filename, LinAnisoScat, Nuc->getAptr(), ind, FBData, disab ) );
            } else { std::cout << "Error, mubar = " << mubar << " is unphysical."; throw; }
          } else { std::cout << " unknown scattering type " << scat_type << " in Nuclide " << name << std::endl; }
        } else if ( r.attribute("Type") and r.attribute("Distribution") ) {
          std::cout << " Error, you have defined both the scattering type and a distribution. Please enter only one. " << std::endl;
        }
        else {
          std::cout << " unknown scattering Distribution " << dist_name << " in Nuclide " << name << std::endl;
          throw;
        }
      } else if ( rxn_type == "Fission" ) {
        std::shared_ptr< fission_reaction > FissRxn;
        std::string chi_dist_name;
        std::string mult_dist_name = r.attribute("Multiplicity").value();
        std::shared_ptr< Distribution<int> > multDist = findByName( int_Distributions, mult_dist_name );
        if ( multDist ) {
          FissRxn = std::make_shared< fission_reaction > ( a, b, xs_filename, multDist, ind, FBData, disab );
          if ( r.attribute("Chi") ) {
            chi_dist_name  = r.attribute("Chi").value();
            std::shared_ptr< Distribution<double> > chiDist = findByName( double_Distributions, chi_dist_name );
            if ( ! chiDist ) { std::cout << " unknown chi Distribution " << chi_dist_name << " in Nuclide " << name << std::endl; throw; }
            FissRxn->setChiDistribution( chiDist );
          }
          FissRxn->setProblemType( Geom->getType() );
          Nuc->addReaction( FissRxn );
          Nuc->addFissionReaction( FissRxn );
        }
        else {
          std::cout << " unknown multiplicity Distribution " << mult_dist_name << " in Nuclide " << name << std::endl;
          throw;
        }
      }
      else {
        std::cout << "unknown reaction type " << rxn_type << std::endl;
        throw;
      }
    }
  } 
  
  // iterate over Materials
  std::vector< std::shared_ptr<Material> > Materials;
  pugi::xml_node input_Materials = root_node.child("Materials");
  for ( auto m : input_Materials ) {
    std::string   name = m.attribute("Name").value();
    double        aden = m.attribute("Density").as_double();
    
    std::shared_ptr< Material > Mat = std::make_shared< Material > ( name, aden );
    if ( m.attribute("Temperature") ) { Mat->setTemp( m.attribute("Temperature").as_double() ); };
    Materials.push_back( Mat );

    // iterate over Nuclides
    for ( auto n : m.children() ) {
      if ( (std::string) n.name() == "Nuclide" ) {
        std::string Nuclide_name = n.attribute("Name").value();
        double      frac         = n.attribute("NumberFraction").as_double();
        
        std::shared_ptr<Nuclide> N = findByName( Nuclides, Nuclide_name );
        Mat->addNuclide( N, frac );
        if ( N->is_fissionable() ) { Mat->addFissionableNuclide( N, frac ); }
      }
    }
  }
  
  // iterate over Surfaces
  std::vector< std::shared_ptr< Surface > > Surfaces;
  pugi::xml_node input_Surfaces = root_node.child("Surfaces");
  for ( auto s : input_Surfaces ) {
    std::string type = s.name();

    std::shared_ptr< Surface > S;
    if ( type == "Plane" ) {
      std::string name = s.attribute("Name").value();
      double      a    = s.attribute("a").as_double();
      double      b    = s.attribute("b").as_double();
      double      c    = s.attribute("c").as_double();
      double      d    = s.attribute("d").as_double();
      S = std::make_shared< plane > ( name, a, b, c, d );
    } else if ( type == "Sphere" ) {
      std::string name = s.attribute("Name").value();
      double      x    = s.attribute("x").as_double();
      double      y    = s.attribute("y").as_double();
      double      z    = s.attribute("z").as_double();
      double      r    = s.attribute("r").as_double();
      S = std::make_shared< sphere > ( name, x, y, z, r );
    } else if ( type == "CylinderX" ) {
      std::string name = s.attribute("Name").value();
      double      y    = s.attribute("y").as_double();
      double      z    = s.attribute("z").as_double();
      double      r    = s.attribute("r").as_double();
      S = std::make_shared< x_cylinder > ( name, y, z, r );
    } else if ( type == "CylinderZ" ) {
      std::string name = s.attribute("Name").value();
      double      x    = s.attribute("x").as_double();
      double      y    = s.attribute("y").as_double();
      double      r    = s.attribute("r").as_double();
      S = std::make_shared< z_cylinder > ( name, x, y, r );
    } else if ( type == "ConeX" ) {
      std::string name = s.attribute("Name").value();
      double      x    = s.attribute("x").as_double();
      double      y    = s.attribute("y").as_double();
      double      z    = s.attribute("z").as_double();
      double      r    = s.attribute("r").as_double();
      S = std::make_shared< x_cone > ( name, x, y, z, r );
    } else {
      std::cout << " unknown Surface type " << type << std::endl;
      throw;
    }
    // handle reflecting boundary conditions
    if ( (std::string) s.attribute("BC").value() == "Reflect" ) {
      S->makeReflecting();
    }
    Surfaces.push_back( S );
  }

  // iterate over Cells
  std::vector< std::shared_ptr< Cell > > Cells;
  pugi::xml_node input_Cells = root_node.child("Cells");
  for ( auto c : input_Cells ) {
    std::string name = c.attribute("Name").value();

    std::shared_ptr< Cell > Cel = std::make_shared< Cell > ( name );
    Cells.push_back( Cel );

    // Cell Material
    if ( c.attribute("Material") ) {
      std::shared_ptr< Material > matPtr = findByName( Materials, c.attribute("Material").value() );
      if ( matPtr ) {
        Cel->setMaterial( matPtr );
      } else {
        std::cout << " unknown Material " << c.attribute("Material").value() << " in Cell " << name << std::endl;
        throw;
      } 
   } else {
    std::shared_ptr< Nuclide > Nuc = std::make_shared< Nuclide > ( "vacuum" );
    Nuc->addReaction( std::make_shared< capture_reaction > ( std::numeric_limits<double>::epsilon(), 0.0, "blank", 0, nullptr, true) );
    std::shared_ptr< Material > Mat = std::make_shared< Material > ( "void", std::numeric_limits<double>::epsilon() );
    Mat->addNuclide( Nuc, 1.0 );
    Cel->setMaterial( Mat );
   }

    // Cell importance
    if ( c.attribute("Importance") ) {
      Cel->setImportance( c.attribute("Importance").as_double() );
    }

    // Cell volume
    if ( c.attribute("Volume") ) {
      Cel->setVolume( c.attribute("Volume").as_double() );
    }
   
    // iterate over Surfaces
    for ( auto s : c.children() ) {
      if ( (std::string) s.name() == "Surface" ) {
        std::string name  = s.attribute("Name").value();
        int         sense = s.attribute("Sense").as_int();

        std::shared_ptr< Surface > SurfPtr = findByName( Surfaces, name );
        if ( SurfPtr ) {
          Cel->addSurface( findByName( Surfaces, name ), sense );
        }
        else {
          std::cout << " unknown Surface with name " << name << std::endl;
          throw;
        }
      }
      else {
        std::cout << " unknown data type " << s.name() << " in Cell " << name << std::endl;
        throw;
      }
    }
  }
  
  // Build cell neighbor list for speed up
  bool already_added = false;
  for ( auto c : Cells ) {
    for ( auto s : c->getSurfaces() ) {
      for ( auto c2 : Cells ) {
        for ( auto s2 : c2->getSurfaces() ) {
          if ( s.first->name() == s2.first->name() ) {
            for ( auto c3 : c->getNeighbors() ) {
              if ( c2->name() == c3->name() ) { already_added = true; }
            }
            if ( already_added ) {
              already_added = false;
            } else {
              c->addNeighbor( c2 );
            }
          }
        }
      }
    }
  }

  // iterate over Estimators
  std::vector< std::shared_ptr< Estimator > > Estimators;
  // std::vector< std::shared_ptr< flux_estimator > > SingleValueCellEstimators;
  std::vector< std::shared_ptr< Mesh > > MeshTallies;
  pugi::xml_node input_Estimators = root_node.child("Estimators");
  for ( auto e : input_Estimators ) {
    std::string type = e.name();
    std::string name = e.attribute("Name").value();
    
    if ( type == "Current" ) {
      std::shared_ptr< surface_current_estimator > Est = std::make_shared< surface_current_estimator > ( name );

      // get the Surfaces
      for ( auto s : e.child("Surfaces") ) {
        if ( (std::string) s.name() == "Surface" ) {
          std::string surfacename = s.attribute("Name").value();
          std::shared_ptr< Surface > SurfPtr = findByName( Surfaces, surfacename );
          if ( e.attribute("Name") ) {
            std::string name = e.attribute("Name").value();
          } else {
            std::string name = surfacename;
          }
          if ( SurfPtr ) {
            SurfPtr->attachEstimator( Est );
          }
          else {
            std::cout << " unknown Surface label " << surfacename << " in Estimator " << name << std::endl;
          }
        }
      }
      Estimators.push_back( Est );
    }
    else if ( type == "CountingSurface" ) {
      std::shared_ptr< counting_estimator > Est = std::make_shared< counting_estimator > ( name );

      // get the Surfaces
      for ( auto s : e.child("Surfaces") ) {
        if ( (std::string) s.name() == "Surface" ) {
          std::string surfacename = s.attribute("Name").value();
          std::shared_ptr< Surface > SurfPtr = findByName( Surfaces, surfacename );
          if ( e.attribute("Name") ) {
            std::string name = e.attribute("Name").value();
          } else {
            std::string name = surfacename;
          }
          if ( SurfPtr ) {
            SurfPtr->attachEstimator( Est );
          }
          else {
            std::cout << " unknown Surface label " << surfacename << " in Estimator " << name << std::endl;
          }
        }
      }
      Estimators.push_back( Est );
    } else if ( type == "TrackLengthFlux" ) {
      std::shared_ptr< track_length_estimator > Est;
      std::vector< std::shared_ptr< Cell > > CellList;
      for ( auto c : e.children() ) {
        if ( (std::string) c.name() == "Binning" ) {
          bool log_flag = false;
          if ( ! c.attribute("Type") ) { std::cout << "Error, specify type for binning estimator " << name << "." << std::endl; throw; }
          std::string type = c.attribute("Type").value();
          if ( type != "Time" && type != "Energy" ) { std::cout << "Error, type " << type << " for binning estimator " << name << " not recognized." << std::endl; throw; }
          if ( c.attribute("Spacing") ) { if ( (std::string)c.attribute("Spacing").value() == "Logarithmic" ) { log_flag = true; }; }
          double min = c.attribute("Minimum").as_double();
          double max = c.attribute("Maximum").as_double();
          double n_intervals = c.attribute("NumberOfIntervals").as_double();
          Est = std::make_shared< counting_track_length_estimator > ( name, type, min, max, n_intervals, log_flag );
        } else {
          Est = std::make_shared< track_length_estimator > ( name );
        }
      }
      // get the Cells
      for ( auto c : e.child("Cells") ) {
        if ( (std::string) c.name() == "Cell" ) {
          std::string cellname = c.attribute("Name").value();
          std::shared_ptr< Cell > CelPtr = findByName( Cells, cellname );
          CellList.push_back( CelPtr );
          if ( e.attribute("Name") ) {
            std::string name = e.attribute("Name").value();
          } else {
            std::string name = cellname;
          }
          if ( CelPtr ) {
            if ( e.attribute("Reaction") ) {
              std::string rxn = e.attribute("Reaction").value();
              if ( rxn == "Capture" ) {
                Est->setReaction( rxn );
              } else if ( rxn == "Scatter" ) {
                Est->setReaction( rxn );
              } else if ( rxn == "Fission" ) {
                Est->setReaction( rxn );
              } else { std::cout << " unknown reaction type " << rxn << " in Estimator " << e.attribute("Name").value() << std::endl; throw; }
            }
            Est->addCellList( CellList );
            CelPtr->attachEstimator( Est );
            // SingleValueCellEstimators.push_back( Est );
          }
          else {
            std::cout << " unknown Cell label " << cellname << " in Estimator " << name << std::endl;
          }
        }
      }
      Estimators.push_back( Est );
    } else if ( type == "CollisionFlux" ) {
      std::shared_ptr< collision_estimator > Est;
      for ( auto c : e.children() ) {
        if ( (std::string) c.name() == "Binning" ) {
          bool log_flag = false;
          if ( ! c.attribute("Type") ) { std::cout << "Error, specify type for binning estimator " << name << "." << std::endl; throw; }
          std::string type = c.attribute("Type").value();
          if ( type != "Time" && type != "Energy" ) { std::cout << "Error, type " << type << " for binning estimator " << name << " not recognized." << std::endl; throw; }
          if ( c.attribute("Spacing") ) { if ( (std::string)c.attribute("Spacing").value() == "Logarithmic" ) { log_flag = true; }; }
          double min = c.attribute("Minimum").as_double();
          double max = c.attribute("Maximum").as_double();
          double n_intervals = c.attribute("NumberOfIntervals").as_double();
          Est = std::make_shared< counting_collision_estimator > ( name, type, min, max, n_intervals, log_flag );
        } else {
          Est = std::make_shared< collision_estimator > ( name );
        }
      }
      // get the Cells
      for ( auto c : e.child("Cells") ) {
        if ( (std::string) c.name() == "Cell" ) {
          std::string cellname = c.attribute("Name").value();
          std::shared_ptr< Cell > CelPtr = findByName( Cells, cellname );
          if ( e.attribute("Name") ) {
            std::string name = e.attribute("Name").value();
          } else {
            std::string name = cellname;
          }
          if ( CelPtr ) {
            if ( e.attribute("Reaction") ) {
              std::string rxn = e.attribute("Reaction").value();
              if ( rxn == "Capture" ) {
                Est->setReaction( rxn );
              } else if ( rxn == "Scatter" ) {
                Est->setReaction( rxn );
              } else if ( rxn == "Fission" ) {
                Est->setReaction( rxn );
              } else { std::cout << " unknown reaction type " << rxn << " in Estimator " << e.attribute("Name").value() << std::endl; throw; }
            }
            CelPtr->attachEstimator( Est );
            // SingleValueCellEstimators.push_back( Est );
          }
          else {
            std::cout << " unknown Cell label " << cellname << " in Estimator " << name << std::endl;
          }
        }
      }
      Estimators.push_back( Est );
    }else if ( type == "PulseHeight" ) {
      std::shared_ptr< pulse_height_estimator > Est;
      for ( auto c : e.children() ) {
        if ( (std::string) c.name() == "Binning" ) {
          bool log_flag = false;
          if ( c.attribute("Spacing") ) { if ( (std::string)c.attribute("Spacing").value() == "Logarithmic" ) { log_flag = true; }; }
          double min = c.attribute("Minimum").as_double();
          double max = c.attribute("Maximum").as_double();
          double n_intervals = c.attribute("NumberOfIntervals").as_double();
          Est = std::make_shared< pulse_height_estimator > ( name, min, max, n_intervals, log_flag );
        }
      }
      // get the Cells
      for ( auto c : e.child("Cells") ) {
        if ( (std::string) c.name() == "Cell" ) {
          std::string cellname = c.attribute("Name").value();
          std::shared_ptr< Cell > CelPtr = findByName( Cells, cellname );
          if ( e.attribute("Name") ) {
            std::string name = e.attribute("Name").value();
          } else {
            std::string name = cellname;
          }
          if ( CelPtr ) {
            CelPtr->attachEstimator( Est );
          }
          else {
            std::cout << " unknown Cell label " << cellname << " in Estimator " << name << std::endl;
          }
        }
      }
      Estimators.push_back( Est );
	}else if ( type == "MeshTally" ) {
      std::shared_ptr< Mesh > MeshT;
      std::vector< std::shared_ptr< Tally > > TallyList;
      std::vector<double> origin;
      std::vector<double> extent;
      std::vector<int> intervals;
      std::string type;
      std::string output;
      if ( e.attribute("Type") ) {
        type = e.attribute("Type").value();
      } else {
        std::cout << "Error, please specify mesh tally type." << std::endl;
        throw;
      }
      if ( e.attribute("OutputFile") ) {
        output = e.attribute("OutputFile").value();
      } else {
        std::cout << "Error, please specify mesh output file." << std::endl;
        throw;
      }
      for ( auto c : e.children() ) {
        if ( (std::string) c.name() == "Origin" ) {
          origin.push_back( c.attribute("x").as_double() );
          origin.push_back( c.attribute("y").as_double() );
          origin.push_back( c.attribute("z").as_double() );
        }
        if ( (std::string) c.name() == "Tallies" ) {
          for ( auto s : c.children() ) {
            std::string tally_name;
            std::string tally_rxn;
            double tally_multiplier;
            if ( s.attribute("Name") ) {
              tally_name = s.attribute("Name").value();
            } else {
              std::cout << " Error, please name all tallies in a MeshTally."; throw;
            }
            if ( s.attribute("Reaction") ) {
              tally_rxn = s.attribute("Reaction").value();
              if ( ! ( tally_rxn == "Capture" || tally_rxn == "Scatter" || tally_rxn == "Fission" || tally_rxn == "None" ) ) {
                std::cout << " unknown reaction type " << tally_rxn << " in MeshTally." << std::endl; throw;
              }
            } else { tally_rxn = "None"; }
            if ( s.attribute("Multiplier") ) {
              tally_multiplier = s.attribute("Multiplier").as_double();
            } else {
              tally_multiplier = 1.0;
            }
            TallyList.push_back( std::make_shared<Tally>(tally_name, tally_rxn, tally_multiplier) );
          }
        }
      }
      if ( type == "Cartesian" ) {
        for ( auto c : e.children() ) {
          if ( (std::string) c.name() == "Extent" ) {
            extent.push_back( c.attribute("x").as_double() );
            extent.push_back( c.attribute("y").as_double() );
            extent.push_back( c.attribute("z").as_double() );
          }
          if ( (std::string) c.name() == "Intervals" ) {
            intervals.push_back( c.attribute("x").as_int() );
            intervals.push_back( c.attribute("y").as_int() );
            intervals.push_back( c.attribute("z").as_int() );
          }
        }
        MeshT = std::make_shared< CartesianMesh >( output, origin, extent, intervals, TallyList );
      } else if ( type == "Cylindrical" ) {
        for ( auto c : e.children() ) {
          if ( (std::string) c.name() == "Extent" ) {
            extent.push_back( c.attribute("r").as_double() );
            extent.push_back( c.attribute("z").as_double() );
            extent.push_back( c.attribute("theta").as_double() );
          }
          if ( (std::string) c.name() == "Intervals" ) {
            intervals.push_back( c.attribute("r").as_int() );
            intervals.push_back( c.attribute("z").as_int() );
            intervals.push_back( c.attribute("theta").as_int() );
          }
        }
        MeshT = std::make_shared< CylindricalMesh >( output, origin, extent, intervals, TallyList );
      } else { std::cout << " unknown Mesh type " << type << "." << std::endl; throw; }
      if ( e.attribute("OutputFormat") ) {
        std::string outputtype = e.attribute("OutputFormat").value();
        if ( outputtype == "vtu" || outputtype == "raw" ) {
          MeshT->setOutputFormat( outputtype );
        } else { std::cout << " unknown output type " << outputtype << " in MeshTally." << std::endl; throw; }
      }
      MeshTallies.push_back( MeshT );
    } else {
      std::cout << "unknown Estimator type " << name << std::endl;
      throw;
    }
  }
  
  // iterate over Sources
  std::vector< std::shared_ptr< Source > > Sources;
  pugi::xml_node input_Sources = root_node.child("Sources");
  for ( auto s : input_Sources ) {
    
    std::shared_ptr< Source > src;
    
    pugi::xml_node input_Source_position    = s.child("Position");
    pugi::xml_node input_Source_direction   = s.child("Direction");
    pugi::xml_node input_Source_energy      = s.child("Energy");
    pugi::xml_node input_Source_time        = s.child("Time");

    std::string pos_dist_name  = input_Source_position.attribute("Distribution").value();
    std::string dir_dist_name  = input_Source_direction.attribute("Distribution").value();
    std::string e_dist_name    = input_Source_energy.attribute("Distribution").value();
    std::string time_dist_name = input_Source_time.attribute("Distribution").value();

    std::shared_ptr< Distribution< point > >  posDist  = findByName( point_Distributions,  pos_dist_name );
    std::shared_ptr< Distribution< point > >  dirDist  = findByName( point_Distributions,  dir_dist_name );
    std::shared_ptr< Distribution< double > > eDist    = findByName( double_Distributions, e_dist_name );
    std::shared_ptr< Distribution< double > > timeDist = findByName( double_Distributions, time_dist_name );

    if ( ! eDist && s.attribute("Energy") ) 
    {
      double src_energy = s.attribute("Energy").as_double();
      eDist = std::make_shared< arbitraryDelta_Distribution< double > > ( "MonoEnergyDist", src_energy );
    } 
    else 
    {
      if (eDist)
      {
        // do nothing
      }
      else
      {
        eDist = std::make_shared< arbitraryDelta_Distribution< double > > ( "MonoEnergyDist", 2.0 );
      }
    }

    if ( ! timeDist && s.attribute("Time") ) {
      double src_time = s.attribute("Time").as_double();
      timeDist = std::make_shared< arbitraryDelta_Distribution< double > > ( "TimeDist", src_time );
    } else {
      timeDist = std::make_shared< arbitraryDelta_Distribution< double > > ( "TimeDist", 0.0 );
    }

    if ( posDist && dirDist ) {
      src = std::make_shared< Source > ( posDist, dirDist, eDist, timeDist );
    } else if ( posDist && ! s.child("Direction") && s.attribute("Type") ) {
      std::string src_type = s.attribute("Type").value();
      if ( src_type == "Isotropic" ) {
        std::shared_ptr< Distribution< point > > IsoSrc = std::make_shared< isotropicDirection_Distribution > ( "Isotropic Source" );
        src = std::make_shared< Source > ( posDist, IsoSrc, eDist, timeDist );
      }
    }
    else {
      if ( ! posDist ) { std::cout << " unknown position Distribution "  << pos_dist_name << " in Source " << std::endl; }
      if ( ! dirDist ) { std::cout << " unknown direction Distribution " << dir_dist_name << " in Source " << std::endl; }
      
      throw;
    }
    
    if ( ! eDist ) { std::cout << " unknown energy Distribution " << e_dist_name << " in Source " << std::endl; }

    
    // Emission probability
    if ( s.attribute("Probability") ) {
      src->setProbability( s.attribute("Probability").as_double() );
    }

    Sources.push_back( src );
  }
  
  if ( root_node.child("ShannonMesh") ) {
    pugi::xml_node shannon_mesh = root_node.child("ShannonMesh");
    std::vector< std::shared_ptr< Tally > > TallyList;
    std::vector<double> origin;
    std::vector<double> extent;
    std::vector<int> intervals;
    for ( auto s : shannon_mesh ) {
      if ( (std::string) s.name() == "Origin" ) {
        origin.push_back( s.attribute("x").as_double() );
        origin.push_back( s.attribute("y").as_double() );
        origin.push_back( s.attribute("z").as_double() );
      }
      if ( (std::string) s.name() == "Extent" ) {
        extent.push_back( s.attribute("x").as_double() );
        extent.push_back( s.attribute("y").as_double() );
        extent.push_back( s.attribute("z").as_double() );
      }
      if ( (std::string) s.name() == "Intervals" ) {
        intervals.push_back( s.attribute("x").as_int() );
        intervals.push_back( s.attribute("y").as_int() );
        intervals.push_back( s.attribute("z").as_int() );
      }
    }
    std::shared_ptr< ShannonMesh > shannonmesh = std::make_shared< ShannonMesh >( "none", origin, extent, intervals, TallyList );
    Geom->setShannonMesh( shannonmesh );
  }
  
  
  Geom->addSources( Sources );
  Geom->addCells( Cells );
  Geom->addEstimators( Estimators );
  Geom->addMeshList( MeshTallies );
  // Geom->addSVCEstimators( SingleValueCellEstimators );
  
  if ( root_node.child("VarianceReduction"))
  {
    pugi::xml_node input_VarianceReduction = root_node.child("VarianceReduction");
    for ( auto VR : input_VarianceReduction ) 
    {
      std::string type = VR.name();
      std::string name = VR.attribute("Name").value();
      
      if (type == "ForcedCollision")
      {
        for ( auto c : VR.children() )
        {
          std::string cellname = c.attribute("Name").value();
          std::shared_ptr<Cell> CellPtr = findByName (Cells, cellname);
          if (CellPtr)
          {
            CellPtr->setForcedCollision(true);
          }
          else
          {
            std::cout << " Unknown Cell label " << cellname << " in VarianceReduction " << name << std::endl;
          }
        }
      }
   else if (type =="WeightWindow")
      {
		 
		 double upperbound_weight=VR.attribute("Heavy_Weigh_in").as_double();
		 double lowerbound_weight=VR.attribute("Light_Weigh_in").as_double();
	
      for ( auto c : VR.children() )
        {
		  
          std::string cellname = c.attribute("Name").value();
          std::shared_ptr<Cell> CellPtr = findByName (Cells, cellname);
          if (CellPtr)
		  {
			
            CellPtr->setWieghtWindow(true);
			CellPtr->setUpperbound_weight(upperbound_weight);
			CellPtr->setLowerbound_weight(lowerbound_weight);
		  }
          else
          {
            std::cout << " Unknown Cell label " << cellname << " in VarianceReduction " << name << std::endl;
          }
        }
      }
      else if (type == "ImplicitCapture")
      {
        for ( auto n : VR.children() )
        {
          std::string nuclidename = n.attribute("Name").value();
          std::shared_ptr<Nuclide> NuclidePtr = findByName (Nuclides, nuclidename);
          if (NuclidePtr)
          {
            NuclidePtr->setImplicitCapture(true);
          }
          else
          {
            std::cout << " Unknown Nuclide label " << nuclidename << " in VarianceReduction " << name << std::endl;
          }
        }
      }
      else if (type == "ExponentialTransform")
      {
        double alpha = VR.attribute("a").as_double();
        for ( auto c : VR.children() )
        {
          std::string cellname = c.attribute("Name").value();
          std::shared_ptr<Cell> CellPtr = findByName (Cells, cellname);
          if (CellPtr)
          {
            CellPtr->setExponentialTransform(true);
            CellPtr->setTransformParameter(alpha);
          }
          else
          {
            std::cout << " Unknown Cell label " << cellname << " in VarianceReduction " << name << std::endl;
          }
        }
      }
    }
  }
  
  
  return Geom;
}

