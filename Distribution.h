#ifndef _DISTRIBUTION_HEADER_
#define _DISTRIBUTION_HEADER_

#include <iostream>
#include <utility>
#include <vector>
#include <cmath>
#include <cassert>
#include <string>
#include <memory>

#include "Random.h"
#include "Point.h"

template <class T>
class Distribution {
  private:
    std::string Distribution_name;
  protected:
    double nubar;
  public:
     Distribution( std::string label ) : Distribution_name(label) {};          // constructor
    ~Distribution() {};   // destructor

    virtual std::string name() final { return Distribution_name; };
    virtual T sample() = 0;  // dummy function that must be implemented in each case
    void set_Nubar( double nu ) { nubar = nu; }
    double get_Nubar() { return nubar; }
};

template <class T>
class arbitraryDelta_Distribution : public Distribution<T> {
  private:
    T result;
  public:
     arbitraryDelta_Distribution( std::string label, T val ) : Distribution<T>(label), result(val) {};
    ~arbitraryDelta_Distribution() {};

    T sample() { return result; }
};

template <class T>
class arbitraryDiscrete_Distribution : public Distribution<T> {
  private:
     std::vector< std::pair< T, double > > cdf;
  public:
     arbitraryDiscrete_Distribution( std::string label, std::vector< std::pair< T, double > > data );
    ~arbitraryDiscrete_Distribution() {};
     T sample();
};

template < class T >
arbitraryDiscrete_Distribution<T>::arbitraryDiscrete_Distribution( std::string label, std::vector< std::pair< T, double > > data )
 : Distribution<T>(label) {
  // convert pdf to cdf
  double c = 0.0;
  for ( auto d : data ) {
    // first is pointer to data type T and second is pdf input
    cdf.push_back( std::make_pair( d.first, d.second + c ) );
    c += d.second;
  }
}

template < class T >
T arbitraryDiscrete_Distribution<T>::sample() {
  double   r = Urand() * cdf.back().second;
  for ( auto c : cdf ) {
    // first is pointer to data type T and second is cdf
    if ( r < c.second ) { return c.first; };
  }
  assert( false ); // should not get here
  return cdf.back().first; 
}

class delta_Distribution : public Distribution<double> {
  private:
    double a;
  public:
     delta_Distribution( std::string label, double p1 ) : Distribution(label), a(p1) {};
    ~delta_Distribution() {};
    double sample() { return a; };
};

class uniform_Distribution : public Distribution<double> {
  private:
    double a, b;
  public:
     uniform_Distribution( std::string label, double p1, double p2 ) : Distribution(label), a(p1), b(p2) {};
    ~uniform_Distribution() {};
    double sample();
};

class linear_Distribution : public Distribution<double> {
  private:
    double a, b, fa, fb;
  public:
    linear_Distribution( std::string label, double x1, double x2, double y1, double y2 )  
      : Distribution(label), a(x1), b(x2), fa(y1), fb(y2) {};
   ~linear_Distribution() {};
   double sample();
};

class exponential_Distribution : Distribution<double> {
  private:
    double lambda;
  public:
     exponential_Distribution( std::string label ) : Distribution(label) {};
    ~exponential_Distribution() {};

    void set_lambda(double l) { lambda = l; };
    double getlambda() { return lambda; }
    double sample();
};

class truncated_exponential_Distribution : Distribution<double> {
  private:
    double lambda;
    double term1;
    
  public:
    truncated_exponential_Distribution( std::string label, double l, double s );
    ~truncated_exponential_Distribution() {};
    
    double sample();
};

class normal_Distribution : public Distribution<double> {
  private:
    const double twopi = 2.0 * std::acos(-1.0);
    double mu, sigma;
  public:
     normal_Distribution( std::string label, double p1, double p2 ) : Distribution(label), mu(p1), sigma(p2) {};
    ~normal_Distribution() {};
    double sample();
};

class HenyeyGreenstein_Distribution : public Distribution<double> {
  private:
    double a;
  public:
     HenyeyGreenstein_Distribution( std::string label, double p1 ) : Distribution(label), a(p1) {};
    ~HenyeyGreenstein_Distribution() {};
    double sample();
};

class meanMultiplicity_Distribution : public Distribution<int> {
  private:

  public:
     meanMultiplicity_Distribution( std::string label ) : Distribution(label) {};
    ~meanMultiplicity_Distribution() {};
    int sample();
};

class TerrellFission_Distribution : public Distribution<int> {
  private:
    double sigma, b, nu_bar;
    std::vector<double> cdf;
  public:
    TerrellFission_Distribution( std::string label, double p1, double p2, double p3 );
    ~TerrellFission_Distribution() {};
    int sample();
};

class isotropicDirection_Distribution : public Distribution<point> {
  private:
    const double twopi = 2.0 * std::acos(-1.0);
  public:
     isotropicDirection_Distribution( std::string label ) : Distribution(label) {};
    ~isotropicDirection_Distribution() {};
    point sample();
};

class anisotropicDirection_Distribution : public Distribution<point> {
  private:
    const double twopi = 2.0 * std::acos(-1.0);
    double sin_t;

    point axis;
    std::shared_ptr< Distribution<double> > dist_mu;
  public:
     anisotropicDirection_Distribution( std::string label, point p, std::shared_ptr< Distribution<double> > dmu ) 
       : Distribution(label), axis(p), dist_mu(dmu) 
       { axis.normalize(); sin_t = std::sqrt( 1.0 - axis.z * axis.z ); };
    ~anisotropicDirection_Distribution() {};
    point sample();
};

class independentXYZ_Distribution : public Distribution<point> {
  private:
    std::shared_ptr< Distribution<double> > dist_x, dist_y, dist_z;
  public:
     independentXYZ_Distribution( std::string label, std::shared_ptr< Distribution<double> > dx, 
       std::shared_ptr< Distribution<double> > dy, std::shared_ptr< Distribution<double> > dz ) 
       : Distribution(label), dist_x(dx), dist_y(dy), dist_z(dz) {};
    ~independentXYZ_Distribution() {};

    point sample();
};

class uniformSPH_Distribution : public Distribution<point> {
  private:
    std::shared_ptr< Distribution<double> > dist_rsq, dist_mu, dist_phi;
    point location;
  public:
     uniformSPH_Distribution( std::string label, point loc, std::shared_ptr< Distribution<double> > drsq, 
       std::shared_ptr< Distribution<double> > dmu, std::shared_ptr< Distribution<double> > dphi ) 
       : Distribution(label), location(loc), dist_rsq(drsq), dist_mu(dmu), dist_phi(dphi) {};
    ~uniformSPH_Distribution() {};

    point sample();
};

class uniformCYL_Distribution : public Distribution<point> {
  private:
    std::shared_ptr< Distribution<double> > dist_rsq, dist_theta, dist_z;
    double thetaX, thetaY, thetaZ;
    point location;
  public:
     uniformCYL_Distribution( std::string label, point loc, std::shared_ptr< Distribution<double> > drsq, 
       std::shared_ptr< Distribution<double> > dtheta, std::shared_ptr< Distribution<double> > dz,
       double tX, double tY, double tZ ) 
       : Distribution(label), location(loc), dist_rsq(drsq), dist_theta(dtheta), dist_z(dz), thetaX(tX),
       thetaY(tY), thetaZ(tZ) {};
    ~uniformCYL_Distribution() {};

    point sample();
};

class Problem6_Distribution : public Distribution<double> {
  private:
    double a;
  public:
     Problem6_Distribution( std::string label, double p1 ) : Distribution(label), a(p1) {};
    ~Problem6_Distribution() {};
    double sample();
};

class WattFissionSpectrum : public Distribution<double> {
  private:
    double K, L, M, a, b;
  public:
     WattFissionSpectrum( std::string label, double p1, double p2 ) : Distribution(label), a(p1), b(p2) {
       K = 1 + ( b / ( 8 * a ) );
       L = ( K + std::pow( K * K - 1, 0.5 ) ) / a;
       M = a * L - 1;
     };
    ~WattFissionSpectrum() {};
    double sample();
};

#endif
