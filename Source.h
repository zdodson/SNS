#ifndef _SOURCE_HEADER_
#define _SOURCE_HEADER_

#include <stack>
#include <memory>

#include "Point.h"
#include "Distribution.h"
#include "Particle.h"
#include "Cell.h"

class Source {
  private:
    double emission_probability = 1.0; // Emission probability can be changed from 1.0
    std::shared_ptr< Distribution<point> > dist_pos;   // Position distribution
    std::shared_ptr< Distribution<point> > dist_dir;   // Direction distribution
    std::shared_ptr< Distribution<double> > dist_e;    // Energy distribution
    std::shared_ptr< Distribution<double> > dist_time; // Time distribution
  public:
     Source( std::shared_ptr< Distribution<point> > pos, std::shared_ptr< Distribution<point> > dir,
     std::shared_ptr< Distribution<double> > e, std::shared_ptr< Distribution<double> > t )
       : dist_pos(pos), dist_dir(dir), dist_e(e), dist_time(t) {};
    ~Source() {};

    Particle sample( std::vector< std::shared_ptr< Cell > > c_list );
    void setProbability( double prob ) { emission_probability = prob; };
    double getProbability() { return emission_probability; };
};

#endif
