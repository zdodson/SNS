#ifndef _NUCLIDE_HEADER_
#define _NUCLIDE_HEADER_

#include <vector>
#include <string>
#include <memory>

#include "Reaction.h"
#include "Particle.h"
#include "OTF_data.h"

class Nuclide {
  private:
    std::string nuclide_name;
    std::vector< std::shared_ptr< reaction > > rxn;
    std::shared_ptr< fission_reaction > fiss_rxn;
    double* MatTempPtr = nullptr;
    bool fissionable = false;
    bool implicitCapture = false;
    std::shared_ptr<FBotfdata> otf;
  public:
    double AtomicNumber = 1.0;
     Nuclide( std::string label ) : nuclide_name(label) {};
    ~Nuclide() {};

    std::string name() { return nuclide_name; }
    std::vector< std::shared_ptr< reaction > > getReactions() {return rxn;} ;

    void      addReaction( std::shared_ptr< reaction > );
    void addFissionReaction( std::shared_ptr< fission_reaction > R ) { fissionable = true; fiss_rxn = R; }
    void    setMatTempPtr( double* T );
    void setOTFPtr( std::shared_ptr<FBotfdata> FB ) { otf = FB; };
    void             setA( double A) { AtomicNumber = A; };
    double*       getAptr() { return &AtomicNumber; };
    double* getMatTempPtr() { return MatTempPtr; };
    double       total_xs( Particle* p );
    double        fiss_xs( Particle* p );
    double         cap_xs( Particle* p );
    double        scat_xs( Particle* p );
    double    fiss_energy() { return fiss_rxn->sampleFissionE(); };
    double       getNubar() { if (fissionable) { return fiss_rxn->getnubar(); } else { return 0.0; } };
    bool   is_fissionable() { return fissionable; };

    std::shared_ptr< reaction > sample_reaction(Particle* p);
    
    std::shared_ptr< reaction > get_scatter_reaction();
    
    void setImplicitCapture(bool inImplicitCapture) { implicitCapture = inImplicitCapture; }
    bool getImplicitCapture() {return implicitCapture;}
};


#endif
