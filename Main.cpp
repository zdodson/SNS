// Team 1
// NERS 590 (Monte Carlo Software) - Project
#include <iostream>

#include "ProcessInput.h"
#include "Simulation.h"
#ifdef _MPI
  #include "mpi.h"
#endif

// Takes input file name as only required argument,
//followed by number of particles as optional argument (overrides input file)
int main( int argc, char *argv[] ) {
  
  int rank = 0,world_size = 1;
  #ifdef _MPI
    MPI_Init(&argc,&argv);                     // Initialize MPI
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);       // Get rank number ( 0 to (N - 1) )
    MPI_Comm_size(MPI_COMM_WORLD,&world_size); // Get total number of ranks
  #endif
  
  // argc should be 2 for correct execution
  if ( argc > 3 || argc < 2 ) { std::cout<<"usage: "<< argv[0] <<" <filename> [n particles]\n"; exit(1); }
  std::shared_ptr<Geometry> Geom = ProcessInput( argv[1] ); // Set up problem using input file
  if ( argc == 3 ) {
    std::string np = argv[2];
    Geom->set_Nparticles( atoi(np.c_str()) );
  }
  
  // State info
  if (rank == 0) { Geom->printInfo(); }
  
  if ( Geom->getType() == "Fixed Source" ) {
    FixedSourceSimulation( Geom, rank, world_size );
  } else {
    EigenvalueSimulation( Geom, rank, world_size );
  }
  return 0;
}